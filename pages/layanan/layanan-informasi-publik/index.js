import React from "react";
import s from "./infoPublik.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import { Link } from "@mui/material";
import OpenInBrowserIcon from "@mui/icons-material/OpenInBrowser";
import Head from "next/head";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Layanan Informasi Publik"}
        icon={<MiscellaneousServicesIcon fontSize="large" />}
      />
      <div className={s.content}>
        <div className={s.desc}>
          Kewajiban Badan Riset dan Inovasi Daerah Provinsi NTB adalah
          memberikan pelayanan informasi kepada pemohon informasi Publik secara
          cepat, tepat waktu, biaya proporsional dan cara yang mudah. Untuk
          dapat memperoleh salinan informasi yang Anda butuhkan, Anda dapat
          mengisi formulir yang ada dan mengikuti tata cara untuk memperoleh
          informasi publik dengan standar biaya layanan informasi yang berlaku.
        </div>
        <iframe
          src="/layanan/pdf/TATA CARA PERMOHONAN INFORMASI PUBLIK.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
        <div className={s.link}>
          <Link
            href="https://forms.gle/Hys3uDhYvUsXM2uD9"
            style={{ display: "flex", alignItems: "center" }}
          >
            <OpenInBrowserIcon />
            Form Permohonan Informasi Publik
          </Link>
          <Link
            href="/documents/Standar Biaya Layanan Informas - BRIDA NTB.pdf"
            style={{ display: "flex", alignItems: "center" }}
          >
            <OpenInBrowserIcon />
            Standar Biaya Layanan Informasi
          </Link>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
