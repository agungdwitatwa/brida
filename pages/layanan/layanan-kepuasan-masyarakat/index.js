import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import Head from "next/head";
import s from "./kepuasanMasyarakat.module.css";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import VoteWrapper from "../../../components/Vote/VoteWrapper";

export async function getServerSideProps(context) {
  let ip;

  const { req } = context;

  if (req.headers["x-forwarded-for"]) {
    ip = req.headers["x-forwarded-for"].split(",")[0];
  } else if (req.headers["x-real-ip"]) {
    ip = req.connection.remoteAddress;
  } else {
    ip = req.connection.remoteAddress;
  }

  return {
    props: {
      ip,
    },
  };
}

function Index({ ip }) {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Survey Kepuasan Masyarakat"}
        icon={<MiscellaneousServicesIcon fontSize="large" />}
      />
      <div className={s.content}>
        <VoteWrapper ipAddress={ip} />
      </div>
      <div className={s.content}>
        <iframe
          src="https://docs.google.com/forms/d/e/1FAIpQLSdfqjIjsgcCANIlDbvJ7T_S8X2dYPRIJ9E7WMPZ6sLbNJ_4xw/viewform?embedded=true"
          width="100%"
          height="2334"
          frameBorder="0"
          marginHeight="0"
          marginWidth="0"
        >
          Memuat…
        </iframe>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
