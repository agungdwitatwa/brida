import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import Head from "next/head";
import s from "./layananPengaduan.module.css";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import { Link } from "@mui/material";
import OpenInBrowserIcon from "@mui/icons-material/OpenInBrowser";
import Stack from "@mui/material/Stack";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Layanan Pengaduan"}
        icon={<MiscellaneousServicesIcon fontSize="large" />}
      />
      <div className={s.content}>
        <div>
          Layanan Pengaduan BRIDA NTB kini telah terintegrasi dengan NTB Care,
          Kanal pengaduan resmi milik Pemerintah Provinsi Nusa Tenggara Barat
          dan pengaduan secara Nasional melalui Kanal Lapor!
        </div>
        <Stack spacing={2} marginTop={2} direction="row">
          <Link
            href="https://care.ntbprov.go.id/"
            style={{ display: "flex", alignItems: "center" }}
            target="_blank"
          >
            <OpenInBrowserIcon />
            NTB Care
          </Link>
          <Link
            href="https://www.lapor.go.id/"
            style={{ display: "flex", alignItems: "center" }}
            target="_blank"
          >
            <OpenInBrowserIcon />
            Kanal Lapor
          </Link>
          <Link
            href="https://forms.gle/Tg245jn6AKdbynRi6"
            style={{ display: "flex", alignItems: "center" }}
            target="_blank"
          >
            <OpenInBrowserIcon />
            Pengaduan Penyalahgunaan Wewenang
          </Link>
        </Stack>
      </div>
      <div className={s.content}>
        <img
          src="/documents/NTB CARE.jpeg"
          alt="layanan Pengaduan"
          width="100%"
        />
      </div>
      <Footer />
    </div>
  );
}

export default Index;
