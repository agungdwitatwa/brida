import { React, useEffect } from "react";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import Head from "next/head";
import s from "./layananPenelitian.module.css";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import FeedTwoToneIcon from "@mui/icons-material/FeedTwoTone";
import Link from "next/link";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Formulir Isian ijin Penelitian"}
        icon={<MiscellaneousServicesIcon fontSize="large" />}
      />
      <div className={s.content}>
        <div>
          Ingin melakukan penelitian di BRIDA NTB untuk keperluan Tugas Akhir,
          Tugas Kuliah atau kebutuhan lainnya? Anda bisa mengajukan Surat Izin
          Penelitian dengan mengisi Formulir yang telah disediakan dibawah ini.
        </div>
        <Link href="https://docs.google.com/forms/d/e/1FAIpQLSfl8q2A2tiPrl-5zlNsn6NcTz2XhWGVb2tPeC031WOlPSVAUw/viewform">
          <a className={s.form}>
            <FeedTwoToneIcon /> Form Pengajuan Penelitian
          </a>
        </Link>
      </div>
      <div className={s.content}>
        <div>
          DAFTAR IJIN LAPORAN MENURUT PROGRESS
          <br />
          <br />
        </div>
        <iframe
          src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ8h-4XRDBko6qYEiE0e__YvWAI7-UA2IGbuR1ONa8KBM7NHX8h9SqIMHgYtEQQgLKhR6paz_OMtwdR/pubhtml?gid=722121634&amp;single=true&amp;widget=true&amp;headers=false#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"700px"}
          allowFullScreen
        ></iframe>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
