import React from "react";
import s from "./layananKemitraan.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import Head from "next/head";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import Table from "../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaLayanan", label: "Nama Layanan", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaLayanan: "Form Pendaftaran Calon Mitra",
      link: "https://docs.google.com/forms/d/e/1FAIpQLSez3b3nk7t4VwVnSF8yWNvA9qAEt_G-vz_Xa20p6wjLjIiHtg/viewform?usp=sf_link",
    },
    {
      number: "2",
      namaLayanan: "Form Pendaftaran Komunitas Startup",
      link: "https://docs.google.com/forms/d/e/1FAIpQLSfNaRO7iIf-iG4NjIEaOd9PQ-eHaAk9w_MibSAs0qb5hLYjjQ/viewform?usp=sf_link",
    },
  ];
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Layanan Kemitraan dan Inkubasi"}
        icon={<MiscellaneousServicesIcon fontSize="large" />}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} />
      </div>
      <Footer />
    </div>
  );
}

export default Index;
