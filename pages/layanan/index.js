import React, { useRef, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Pagination, Mousewheel } from "swiper";
import s from "./style.module.css";
import InsertLinkIcon from "@mui/icons-material/InsertLink";
import Link from "next/link";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

export default function App() {
  const { ref, inView } = useInView();
  const leftoRigth = useAnimation();
  const rightoLeft = useAnimation();
  const oleftoRigth = useAnimation();
  const orightoLeft = useAnimation();
  const scaleUp = useAnimation();
  const downtoUp = useAnimation();
  useEffect(() => {
    if (inView) {
      leftoRigth.start({
        x: 0,
        opacity: 1,
        transition: {
          duration: 0.8,
        },
      });
      rightoLeft.start({
        x: 0,
        opacity: 1,
        transition: {
          duration: 0.8,
        },
      });
      oleftoRigth.start({
        x: -50,
        opacity: 0,
      });
      orightoLeft.start({
        x: 50,
        opacity: 0,
      });
      scaleUp.start({
        scale: 0.9,
        opacity: 0,
      });
      downtoUp.start({
        y: 60,
        opacity: 0,
      });
    }

    if (!inView) {
      leftoRigth.start({
        x: -50,
        opacity: 0,
      });
      rightoLeft.start({
        x: 50,
        opacity: 0,
      });
      oleftoRigth.start({
        x: 0,
        opacity: 1,
        transition: {
          duration: 0.8,
        },
      });
      orightoLeft.start({
        x: 0,
        opacity: 1,
        transition: {
          duration: 0.8,
        },
      });
      scaleUp.start({
        scale: 1,
        opacity: 1,
        transition: {
          duration: 0.8,
        },
      });
      downtoUp.start({
        y: 0,
        opacity: 1,
        transition: {
          duration: 0.7,
        },
      });
    }
  }, [inView]);
  return (
    <div className={s.container}>
      <Swiper
        direction={"vertical"}
        slidesPerView={1}
        mousewheel={true}
        pagination={{
          clickable: true,
        }}
        modules={[Mousewheel, Pagination]}
        className={s.swiper}
        speed={1000}
      >
        <SwiperSlide className={s.page}>
          <motion.div
            className={s.illustration}
            initial={{ scale: 0.8, opacity: 0 }}
            animate={scaleUp}
            ref={ref}
          >
            <img src="/svg/penelitian.svg"></img>
          </motion.div>
          <motion.div
            className={s.desc}
            initial={{ y: 60, opacity: 0 }}
            animate={downtoUp}
          >
            <div className={s.back}>
              <Link href="/">
                <a>
                  <KeyboardBackspaceIcon />
                  &nbsp; Kembali ke Beranda
                </a>
              </Link>
            </div>
            <div className={s.title}>
              <div className={s.logo}>
                <img src="/logo pemprov.png" />
                <img src="/logo BRIN.png" />
              </div>
              <div className={s.text}>
                <span>Layanan&nbsp;</span>
                <span>Penelitian</span>
              </div>
            </div>
            <p>
              Ingin melakukan penelitian di BRIDA NTB untuk keperluan Tugas
              Akhir, Tugas Kuliah atau kebutuhan lainnya? Anda bisa mengajukan
              Surat Izin Penelitian dengan mengisi Formulir yang telah
              disediakan dibawah ini.
            </p>
            <div className={s.link}>
              <Link href="#">
                <a>
                  <InsertLinkIcon fontSize="large" /> &nbsp;Ajukan&nbsp;
                  <span>Layanan</span>
                </a>
              </Link>
            </div>
          </motion.div>
        </SwiperSlide>
        <SwiperSlide className={s.page}>
          <motion.div
            className={s.illustration}
            initial={{ x: -100, opacity: 0 }}
            animate={leftoRigth}
            ref={ref}
          >
            <img src="/svg/mitra.svg"></img>
          </motion.div>
          <motion.div
            className={s.desc}
            initial={{ x: 100, opacity: 0 }}
            animate={rightoLeft}
          >
            <div className={s.back}>
              <Link href="/">
                <a>
                  <KeyboardBackspaceIcon />
                  &nbsp; Kembali ke Beranda
                </a>
              </Link>
            </div>
            <div className={s.title}>
              <div className={s.logo}>
                <img src="/logo pemprov.png" />
                <img src="/logo BRIN.png" />
              </div>
              <div className={s.text}>
                <span>Layanan&nbsp;</span>
                <span>Mitra & Inkubasi</span>
              </div>
            </div>
            <p>
              Ingin melakukan penelitian di BRIDA NTB untuk keperluan Tugas
              Akhir, Tugas Kuliah atau kebutuhan lainnya? Anda bisa mengajukan
              Surat Izin Penelitian dengan mengisi Formulir yang telah
              disediakan dibawah ini.
            </p>
            <div className={s.link}>
              <Link href="#">
                <a>
                  <InsertLinkIcon fontSize="large" /> &nbsp;Ajukan&nbsp;
                  <span>Layanan</span>
                </a>
              </Link>
            </div>
          </motion.div>
        </SwiperSlide>
        <SwiperSlide className={s.page}>asda</SwiperSlide>
      </Swiper>
    </div>
  );
}
