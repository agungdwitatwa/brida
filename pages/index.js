import React from "react";
import Header from "../components/Header/header";
import s from "./index.module.scss";
import Head from "next/head";
import Navbar from "../components/Navbar/navbar";
import Carousel from "../components/Carousel/Carousel";
import LightbulbIcon from "@mui/icons-material/Lightbulb";
import BiotechIcon from "@mui/icons-material/Biotech";
import GroupsIcon from "@mui/icons-material/Groups";
import SchoolIcon from "@mui/icons-material/School";
import Card from "../components/Card/Berita";
import Link from "next/link";
import Footer from "../components/Footer/Footer";
import { createClient } from "contentful";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
    accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
  });
  const berita1 = await client.getEntries({
    content_type: "berita",
    select: "fields",
    limit: 4,
    order: "-fields.tanggalBerita",
  });
  const berita2 = await client.getEntries({
    content_type: "berita",
    skip: 4,
    limit: 4,
    select: "fields",
    order: "-fields.tanggalBerita",
  });

  return {
    props: {
      berita: berita1.items,
      berita2: berita2.items,
    },
  };
}

export default function Home({ berita, berita2 }) {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
        <meta
          name="google-site-verification"
          content="iXVqEBK040zjIyXaQCU107jt80RurPFR17u8AaPz8ks"
        />
        <meta
          name="keywords"
          content="BRIDA NTB, Brida NTB, Badan Riset dan Inovasi Daerah NTB, Riset, Riset NTB"
        />
      </Head>

      <Header />
      <Navbar />
      <Carousel />
      {/* Row pertama link ke masing - masing bidang -> /profil/profil-kepala-bidang */}
      <div className={s.bidang}>
        <Link href="/profil/profil-bidang">
          <a>
            <div className={s.item}>
              <LightbulbIcon />
              <div className={s.title}>
                <span>Bidang </span>
                <span>Penelitian Pengembangan Inovasi dan Teknologi</span>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/profil/profil-bidang">
          <a>
            <div className={s.item}>
              <BiotechIcon />
              <div className={s.title}>
                <span>Bidang </span>
                <span>
                  Pemanfaatan <br /> Riset dan Inovasi
                </span>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/profil/profil-bidang">
          <a>
            <div className={s.item}>
              <GroupsIcon />
              <div className={s.title}>
                <span>Bidang </span>
                <span>
                  Kemitraan <br /> dan Inkubasi Bisnis
                </span>
              </div>
            </div>
          </a>
        </Link>
        <Link href="/profil/profil-bidang">
          <a>
            <div className={s.item}>
              <SchoolIcon />
              <div className={s.title}>
                <span>Bidang </span>
                <span>
                  Pengembangan Sumberdaya Ilmu Pengetahuan dan Teknologi
                </span>
              </div>
            </div>
          </a>
        </Link>
      </div>
      <div className={s.content_container}>
        <div className={s.title}>
          <div>BERITA TERBARU</div>
          <div className={s.line}></div>
        </div>
        <div className={`${s.content} ${s.berita_container}`}>
          {berita.map((item, index) => (
            <Card
              className={s.berita_card}
              judulBerita={item.fields.judulBerita}
              cover={item.fields.thumbnail.fields.file.url}
              key={index}
              link={item.fields.slug}
            />
          ))}
        </div>
        <div className={`${s.content} ${s.berita_container}`}>
          {berita2.map((item, index) => (
            <Card
              className={s.berita_card}
              judulBerita={item.fields.judulBerita}
              cover={item.fields.thumbnail.fields.file.url}
              key={index}
              link={item.fields.slug}
            />
          ))}
        </div>
      </div>
      <div className={s.content_container}>
        <div className={s.title}>
          <div>VIDEO BRIDA NTB</div>
          <div className={s.line}></div>
        </div>
        <div className={s.content}>
          <iframe
            width="100%"
            height="400"
            src="https://www.youtube.com/embed/qMYoHUiSJAQ"
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className={s.iframe}
          ></iframe>
        </div>
        <div className={`${s.content} ${s.video_container}`}>
          <iframe
            width="400"
            height="315"
            src="https://www.youtube.com/embed/Ng2Yvj1lxfA"
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className={s.video}
          ></iframe>
          <iframe
            width="400"
            height="315"
            src="https://www.youtube.com/embed/eiKfeaOHxNw"
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className={s.video}
          ></iframe>
          <iframe
            width="400"
            height="315"
            src="https://www.youtube.com/embed/_H99-pvyyS4"
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className={s.video}
          ></iframe>
        </div>
      </div>
      <Footer />
    </div>
  );
}
