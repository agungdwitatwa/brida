import React from "react";
import s from "./profilSekban.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import Head from "next/head";
import FacebookIcon from "@mui/icons-material/Facebook";
import EmailIcon from "@mui/icons-material/Email";
import InstagramIcon from "@mui/icons-material/Instagram";
import Link from "next/link";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Profil Sekretaris Badan | BRIDA NTB - Badan Riset dan Inovasi Daerah
          Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Profil Sekretaris Badan"}
        icon={<AssignmentIndIcon fontSize="large" />}
      />
      <div className={`${s.content} ${s.dataDiri}`}>
        <div className={s.foto}>
          <div className={s.image}>
            <img src="/foto/busekban.jpg" width={"100%"} />
          </div>
        </div>
        <div className={s.desc}>
          <div className={s.nama}>Retno Untari, S.Si, M.Kes</div>
          <div className={s.detail}>
            <table>
              <tbody>
                <tr>
                  <td>NIP</td>
                  <td>:</td>
                  <td>197202101997032005</td>
                </tr>
                <tr>
                  <td>Tempat, Tanggal Lahir</td>
                  <td>:</td>
                  <td>Klaten, 10 Pebruari 1972</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>:</td>
                  <td>Perempuan</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>:</td>
                  <td>Islam</td>
                </tr>
                <tr>
                  <td>Pangkat / Golongan</td>
                  <td>:</td>
                  <td>Pembina Tingkat I / IV b</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>hatimutiara40@yahoo.com</td>
                </tr>
                <tr>
                  <td>Facebook</td>
                  <td>:</td>
                  <td>-</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={s.sosmed}>
            <Link href="#">
              <a>
                <div className={s.item}>
                  <FacebookIcon />
                </div>
              </a>
            </Link>
            <Link href="mailto:hatimutiara40@yahoo.com">
              <a>
                <div className={s.item}>
                  <EmailIcon />
                </div>
              </a>
            </Link>
            <Link href="#">
              <a>
                <div className={s.item}>
                  <InstagramIcon />
                </div>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div className={`${s.content} ${s.content_detail}`}>
        <div className={s.card}>
          <div className={s.title}>Riwayat Jabatan :</div>
          <ol>
            <li>Kepala Balai Hiperkes NTB</li>
            <li>Kepala Bidang Penelitian BLHP NTB</li>
            <li>Kepala Bidang Litbang Bappeda NTB</li>
            <li>Sekretaris Bappeda NTB</li>
            <li>Kabid SDM RSUP NTB</li>
            <li>Kepala Bidang Pengawasan Disnaker NTB</li>
            <li>Sekretaris Brida NTB</li>
          </ol>
        </div>
        <div className={s.nested_card}>
          <div className={s.card}>
            <div className={s.title}>Riwayat Pendidikan :</div>
            <ol>
              <li>SMA 1 Muhammadiyah Klaten (1989)</li>
              <li>S1 Kimia Universitas Gadjah Mada (1995)</li>
              <li>S2 Kesehatan Kerja Universitas Gadjah Mada (2004)</li>
            </ol>
          </div>
          <div className={s.card}>
            <div className={s.title}>Riwayat Penghargaan :</div>
            <ol>
              <li>-</li>
            </ol>
          </div>
          <div className={s.card}>
            <div className={s.title}>Informasi LKHPN :</div>
            <div className={s.link}>
              <a href="/documents/sekban/LHKPN_174388_RETNO UNTARI.pdf">
                Klik Disini
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default index;
