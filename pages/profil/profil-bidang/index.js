import React from "react";
import s from "./profilBidang.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import Head from "next/head";
import Link from "next/link";
import FacebookIcon from "@mui/icons-material/Facebook";
import EmailIcon from "@mui/icons-material/Email";
import InstagramIcon from "@mui/icons-material/Instagram";
import Footer from "../../../components/Footer/Footer";
import { Chip, Divider } from "@mui/material";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Profil Bidang | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa
          Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Profil Bidang"}
        icon={<AssignmentIndIcon fontSize="large" />}
      />
      <Divider>
        <Chip label="Bidang Penelitian Pengembangan Inovasi dan Teknologi" />{" "}
      </Divider>
      <div className={s.content}>
        <div className={s.kabid}>
          <div className={s.foto}>
            <img src="/profil/images/kabid/suryadi.jpg" width={"100%"} />
            <div>
              <div className={s.sosmed}>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <FacebookIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <EmailIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <InstagramIcon />
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className={s.detail}>
            <div className={s.deskripsiBidang}>
              <div className={s.title}>Preview Singkat Bidang</div>
              <div>
                Sebagai Bidang yang memfasilitasi masyarakat dan OPD terkait,
                Bidang Penelitian, Pengembangan Inovasi dan Teknologi telah
                berhasil memfasilitasi lebih dari 100 unit Prototype pemesinan
                dan melakukan kerjasama dengan stakeholder seperti Kedutaan
                Denmark dan Geo Trash Management.
              </div>
            </div>
            <div className={s.title}>
              Kepala Bidang Penelitian Pengembangan Inovasi dan Teknologi
            </div>
            <div className={s.isi}>
              <table>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Lalu Suryadi S. SP.MM.</td>
                  </tr>
                  <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>196912311998031055</td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>:</td>
                    <td>Lombok Timur, 31 Desember 1969</td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>Laki-Laki</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>layadhie@yahoo.co.id</td>
                  </tr>
                  <tr>
                    <td>Pangkat / Golongan</td>
                    <td>:</td>
                    <td>Pembina Tingkat I / IV b</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className={s.kasubid_container}>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/Mahmud Husyairi.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Penelitian dan Pengembangan
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>Mahmud Husyairi, SP.</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>197110242008011009</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Pohgading, 24 Oktober 1971</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Laki-Laki</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>husyairi71@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Tingkat I / III d</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img src="/profil/images/kasubid/Bu Dhani.jpg" width={"100%"} />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Inovasi dan Teknologi
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>Baiq Dhani Sufia Hartati, ST</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>198008052010012005</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Mantang, 5 Agustus 1980</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Perempuan</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>bq.dhanihartati@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Tingkat I / III d</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Divider>
        <Chip label=" Bidang Pemanfaatan Riset dan Inovasi" />
      </Divider>
      <div className={s.content}>
        <div className={s.kabid}>
          <div className={s.foto}>
            <img src="/profil/images/kabid/bulale.jpeg" width={"100%"} />
            <div>
              <div className={s.sosmed}>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <FacebookIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <EmailIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <InstagramIcon />
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className={s.detail}>
            <div className={s.deskripsiBidang}>
              <div className={s.title}>Preview Singkat Bidang</div>
              <div>
                Bidang Pemanfaatan Riset dan Inovasi sebagai etalase Iptek dan
                pusat Inovasi NTB serta Pengelolaan Kawasan Edusiwata Teknologi
              </div>
            </div>
            <div className={s.title}>
              Kepala Bidang Pemanfaatan Riset dan Inovasi
            </div>
            <div className={s.isi}>
              <table>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Lale Ira Amrita Sari, ST</td>
                  </tr>
                  <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>197704142009012004</td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>:</td>
                    <td>Mataram, 14 April 1977</td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>Perempuan</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>laleiraamritasari@gmail.com</td>
                  </tr>
                  <tr>
                    <td>Pangkat / Golongan</td>
                    <td>:</td>
                    <td>PENATA Tingkat I / III d</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className={s.kasubid_container}>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/Hendra Apriana.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Desiminasi Hasil Inovasi Riset dan Teknologi
                Wisata Keilmuan dan Teknologi
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>Hendra Apriana, ST</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>198004052009062006</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Selong Lombok Timur, 5 April 1980</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Perempuan</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>eenkahim@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Tingkat I / III d</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/Nurnianingsih.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Eduwisata keilmuan dan teknologi
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>Nurnianingsih</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>197404212008012019</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Mataram, 21 April 1974</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Perempuan</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>ningsasih19@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Tingkat I / III d</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Divider>
        <Chip
          label="Bidang Pengembangan Sumber Daya Ilmu Pengetahuan dan
              Teknologi"
        />
      </Divider>
      <div className={s.content}>
        <div className={s.kabid}>
          <div className={s.foto}>
            <img src="/profil/images/kabid/ahmadMuslim.jpg" width={"100%"} />
            <div>
              <div className={s.sosmed}>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <FacebookIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <EmailIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <InstagramIcon />
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className={s.detail}>
            <div className={s.deskripsiBidang}>
              <div className={s.title}>Preview Singkat Bidang</div>
              <div>
                Program Beasiswa NTB dan Rumah Bahasa merupakan salah satu tugas
                dari Bidang Pengembangan Sumber Daya Ilmu Pengetahuan dan
                Teknologi selain memfasilitasi Masyarakat dan OPD terkait dalam
                Uji kompetensi profesi dan Standarisasi dan Sertifikasi Produk
              </div>
            </div>
            <div className={s.title}>
              Kepala Bidang Pengembangan Sumber Daya Ilmu Pengetahuan dan
              Teknologi
            </div>
            <div className={s.isi}>
              <table>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Ahmad Muslim, S.Pd</td>
                  </tr>
                  <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>197605102001121001</td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>:</td>
                    <td>Seganteng, 10 Mei 1976</td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>Laki-Laki</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>abiygfra@gmail.com</td>
                  </tr>
                  <tr>
                    <td>Pangkat / Golongan</td>
                    <td>:</td>
                    <td>IV b</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className={s.kasubid_container}>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img src="/profil/images/kasubid/IMG_0788.JPG" width={"100%"} />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Sertifikasi dan Standarisasi
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>Husnul Khuluq, S.Si</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>198708082011011002</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Pepekat, 8 Agustus 1987</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Laki-Laki</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>khuluq.hasan@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata TK I/ IIIc</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/Sri Hastuti.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Peningkatan Kapasitas Sumber Daya Ilmu
                Pengetahuan dan Teknologi
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>SRI HASTUTI NOVILA ANGGRAINI SAIFUL, M.TESOL</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>199311272019032022</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Lombok Timur, 27 November 1993</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Perempuan</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>srihastutinovila@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Muda Tingkat I / III b</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Divider>
        <Chip label="Bidang Kemitraan dan Inkubasi Bisnis" />
      </Divider>
      <div className={s.content}>
        <div className={s.kabid}>
          <div className={s.foto}>
            <img src="/profil/images/kabid/iskandar.jpg" width={"100%"} />
            <div>
              <div className={s.sosmed}>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <FacebookIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <EmailIcon />
                    </div>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <div className={s.item}>
                      <InstagramIcon />
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className={s.detail}>
            <div className={s.deskripsiBidang}>
              <div className={s.title}>Preview Singkat Bidang</div>
              <div>
                Bidang Kemitraan dan Inkubasi bisnis berkomitmen untuk melayani
                dan memfasilitasi masyarakat dari mahasiswa, pelajar, guru dan
                akademisi hingga dunia usaha dalam mengembangkan bisnis melalui
                Program Inkubasi Bisnis dan Kemitraan.
              </div>
            </div>
            <div className={s.title}>
              Kepala Bidang Kemitraan dan Inkubasi Bisnis
            </div>
            <div className={s.isi}>
              <table>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Iskandar Sukmana</td>
                  </tr>
                  <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>197903102010011010</td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>:</td>
                    <td>Ganti, 10 Maret 1979</td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>Laki-Laki</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>anderwalker79@gmail.com</td>
                  </tr>
                  <tr>
                    <td>Pangkat / Golongan</td>
                    <td>:</td>
                    <td> III d</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className={s.kasubid_container}>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/Bintang Rizki.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>Kepala Sub Bidang Inkubasi Bisnis</div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>BINTANG RIZKI SAKINAH, S.IP</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>199406182017082001</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Mataram, 18 Juni 1994</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Perempuan</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>binth13.br@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Penata Muda Tingkat I / III b</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className={s.kasubid}>
            <div className={s.foto}>
              <img
                src="/profil/images/kasubid/H. Makbullah.JPG"
                width={"100%"}
              />
            </div>
            <div className={s.detail}>
              <div className={s.title}>
                Kepala Sub Bidang Kemitraan dan Bisnis Proses
              </div>
              <div className={s.detail_data}>
                <table>
                  <tbody>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td>H. Makbullah, M.Pd</td>
                    </tr>
                    <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td>196902091997021006</td>
                    </tr>
                    <tr>
                      <td>Tempat, Tanggal Lahir</td>
                      <td>:</td>
                      <td>Lombok Barat, 9 Februari 1969</td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td>Laki-Laki</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td>makbul.latib@gmail.com</td>
                    </tr>
                    <tr>
                      <td>Pangkat / Golongan</td>
                      <td>:</td>
                      <td>Pembina / IV a</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default index;
