import React from "react";
import s from "./struktur.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import Head from "next/head";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Struktur Organisasi | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa
          Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Struktur Organisasi"}
        icon={<AssignmentIndIcon fontSize="large" />}
      />
      <div className={s.content}>
        <iframe
          src="/documents/Struktur Organisasi BRIDA NTB.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
      </div>
      <div className={s.content}>
        <iframe
          width="100%"
          height="600"
          frameBorder="0"
          scrolling="no"
          src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR7Ht4z7TiltAyYMbg4Rda2QZ7uBD7Jkk2N4l8HWp_tAmNt-ZlQGp5PVsLwONWNdMiXRPN-fqfBjkTZ/pubhtml?gid=0&single=true"
          allowFullScreen
        ></iframe>
      </div>

      <Footer />
    </div>
  );
}

export default index;
