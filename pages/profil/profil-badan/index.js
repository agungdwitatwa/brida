import React from "react";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import FacebookIcon from "@mui/icons-material/Facebook";
import YouTubeIcon from "@mui/icons-material/YouTube";
import InstagramIcon from "@mui/icons-material/Instagram";
import s from "./profilBadan.module.css";
import Link from "next/link";
import { Link as MaterialLink } from "@mui/material";
import Banner from "../../../components/Banner/Banner";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>Profil | BRIDA Provinsi NTB</title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        icon={<AssignmentIndIcon fontSize="large" />}
        name={"Profil BRIDA Provinsi NTB"}
      />
      <div className={`${s.content} ${s.desc}`}>
        <div className={s.left}>
          <img src="/profil/images/profil.jpg" width={"100%"} />
        </div>
        <div className={s.right}>
          <div className={s.title}>
            Badan Riset dan Inovasi Daerah Provinsi NTB
          </div>
          <div className={s.caption}>
            BRIDA NTB berkomitmen mewujudkan NTB Sejahtera dan Mandiri, melalui
            program-program unggulan dan merangkul masyarakat untuk mandiri
            dengan berkolaborasi.
          </div>
          <h3>Visi - Misi BRIDA Provinsi NTB :</h3>
          <div className={s.caption}>
            Tugas BRIDA NTB sebagai penunjang urusan pemerintahan dalam
            penelitian dan pengembangan merupakan wujud dari kontribusi dalam
            pencapaian Misi Ke-5 NTB Gemilang yaitu NTB “Sejahtera dan Mandiri”
            melalui penanggulangan kemiskinan, mengurangi kesenjangan, dan
            pertumbuhan ekonomi inklusif bertumpu pada pertanian, pariwisata dan
            industrialisasi.
          </div>
          <div className={s.sosmed}>
            <Link href="https://web.facebook.com/BRIDA-Nusa-Tenggara-Barat-103262571862743">
              <a>
                <div className={s.item}>
                  <FacebookIcon />
                </div>
              </a>
            </Link>
            <Link href="https://www.youtube.com/channel/UC080LWwxcuQ1pmSORgDEkWQ">
              <a>
                <div className={s.item}>
                  <YouTubeIcon />
                </div>
              </a>
            </Link>
            <Link href="https://www.instagram.com/brida_ntb/">
              <a>
                <div className={s.item}>
                  <InstagramIcon />
                </div>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div className={`${s.content} ${s.tugas_pokok}`}>
        <div className={s.title}>
          <div>Tugas dan Fungsi Pokok BRIDA NTB</div>
          <div className={s.line}></div>
        </div>
        <div className={s.isi}>
          <div>
            <MaterialLink
              href="/documents/Pergub No. 49 Tahun 2021 - BRIDA.pdf"
              style={{ textDecoration: "none" }}
            >
              Peraturan Gubernur Nomor 49 Tahun 2021
            </MaterialLink>{" "}
            tentang Perubahan Ke Empat Atas Peraturan Gubernur Nomor 51 Tahun
            2016 tentang Kedudukan, Susunan Organisasi, Tugas Dan Fungsi Serta
            Tata Kerja Badan-Badan Daerah Provinsi Nusa Tenggara Barat, serta{" "}
            <MaterialLink
              href="/documents/Perda Nomor 14 Tahun 2021 ttg Perubahan Atas Perda No. 11 Tahun 2016.pdf"
              style={{ textDecoration: "none" }}
            >
              Peraturan Daerah Nomor. 14 Tahun 2021
            </MaterialLink>{" "}
            tentang perubahan kedua atas Peraturan daeran Nomor 11 Tahun 2016,
            kedudukan Badan Riset dan Inovasi Daerah (BRIDA) Provinsi NTB
            mempunyai tugas membantu Gubernur dalam melaksanakan tugas urusan
            fungsi penunjang urusan pemerintahan bidang Penelitian dan
            Pengembangan
          </div>
          <iframe
            src="/documents/Tugas dan Fungsi BRIDA NTB.pdf#view=fitH"
            frameBorder="3"
            width={"100%"}
            height={"500px"}
            allowFullScreen
          ></iframe>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default index;
