import React from "react";
import s from "./profilSekban.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import Head from "next/head";
import FacebookIcon from "@mui/icons-material/Facebook";
import EmailIcon from "@mui/icons-material/Email";
import InstagramIcon from "@mui/icons-material/Instagram";
import Link from "next/link";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Profil Sekretaris Badan | BRIDA NTB - Badan Riset dan Inovasi Daerah
          Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Profil Jabatan Fungsional"}
        icon={<AssignmentIndIcon fontSize="large" />}
      />
      <div className={`${s.content} ${s.dataDiri}`}>
        <div className={s.foto}>
          <div className={s.image}>
            <img
              src="https://drive.google.com/uc?id=1L5E9YA4OqYnA_9iJGvqQ5yTWGgD6mUU7"
              width={"100%"}
            />
          </div>
        </div>
        <div className={s.desc}>
          <div className={s.nama}>Mulyadi Hati Multazam, SP.,M.Si</div>
          <div className={s.detail}>
            <table>
              <tbody>
                <tr>
                  <td>NIP</td>
                  <td>:</td>
                  <td>197212312007011131</td>
                </tr>
                <tr>
                  <td>Tempat, Tanggal Lahir</td>
                  <td>:</td>
                  <td>Lombok Barat, 31 Desember 1972</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>:</td>
                  <td>Laki-Laki</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>:</td>
                  <td>Islam</td>
                </tr>
                <tr>
                  <td>Pangkat / Golongan</td>
                  <td>:</td>
                  <td>Penata TK I / III (d)</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>mulyadi.manggong@gmail.com</td>
                </tr>
                <tr>
                  <td>Facebook</td>
                  <td>:</td>
                  <td>facebook.com/mulyadi.manggong</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={s.sosmed}>
            <Link href="https://www.facebook.com/mulyadi.manggong">
              <a>
                <div className={s.item}>
                  <FacebookIcon />
                </div>
              </a>
            </Link>
            <Link href="mailto:mulyadi.manggong@gmail.com">
              <a>
                <div className={s.item}>
                  <EmailIcon />
                </div>
              </a>
            </Link>
            <Link href="#">
              <a>
                <div className={s.item}>
                  <InstagramIcon />
                </div>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div className={`${s.content} ${s.dataDiri}`}>
        <div className={s.foto}>
          <div className={s.image}>
            {/* <img
              src="https://drive.google.com/uc?id=1L5E9YA4OqYnA_9iJGvqQ5yTWGgD6mUU7"
              width={"100%"}
            /> */}
          </div>
        </div>
        <div className={s.desc}>
          <div className={s.nama}>HASBULLAH, S.Pi</div>
          <div className={s.detail}>
            <table>
              <tbody>
                <tr>
                  <td>NIP</td>
                  <td>:</td>
                  <td>198005042009011003</td>
                </tr>
                <tr>
                  <td>Tempat, Tanggal Lahir</td>
                  <td>:</td>
                  <td>Lotim, 04 Mei 1980</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>:</td>
                  <td>Laki-Laki</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>:</td>
                  <td>Islam</td>
                </tr>
                <tr>
                  <td>Pangkat / Golongan</td>
                  <td>:</td>
                  <td>Penata Muda Tk.I (III/b)</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>hasbul4580@gmail.com</td>
                </tr>
                <tr>
                  <td>Facebook</td>
                  <td>:</td>
                  <td>facebook.com/iisx113</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={s.sosmed}>
            <Link href="https://www.facebook.com/iisx113">
              <a>
                <div className={s.item}>
                  <FacebookIcon />
                </div>
              </a>
            </Link>
            <Link href="mailto:hasbul4580@gmail.com">
              <a>
                <div className={s.item}>
                  <EmailIcon />
                </div>
              </a>
            </Link>
            <Link href="#">
              <a>
                <div className={s.item}>
                  <InstagramIcon />
                </div>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default index;
