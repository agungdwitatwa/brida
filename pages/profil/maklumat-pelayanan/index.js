import React from "react";
import s from "./maklumat.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import HistoryEduIcon from "@mui/icons-material/HistoryEdu";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Maklumat Pelayanan | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa
          Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Maklumat Pelayanan"}
        icon={<HistoryEduIcon fontSize="large" />}
      />
      <div className={s.content}>
        <iframe
          src="/profil/pdf/Maklumat-Pelayanan-BRIDA-NTB.pdf"
          frameBorder="3"
          width={"100%"}
          height={"700px"}
          allowFullScreen
        ></iframe>
      </div>
      <Footer />
    </div>
  );
}

export default index;
