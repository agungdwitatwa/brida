import React from "react";
import s from "./profilKaban.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import Head from "next/head";
import FacebookIcon from "@mui/icons-material/Facebook";
import EmailIcon from "@mui/icons-material/Email";
import InstagramIcon from "@mui/icons-material/Instagram";
import Link from "next/link";

function index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          Profil Kepala Badan | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa
          Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Profil Kepala Badan"}
        icon={<AssignmentIndIcon fontSize="large" />}
      />
      <div className={`${s.content} ${s.dataDiri}`}>
        <div className={s.foto}>
          <div className={s.image}>
            <img src="/foto/pakaban.jpg" width={"100%"} />
          </div>
        </div>
        <div className={s.desc}>
          <div className={s.nama}>H. Wirawan, S.Si., MT.</div>
          <div className={s.detail}>
            <table>
              <tbody>
                <tr>
                  <td>NIP</td>
                  <td>:</td>
                  <td>197410081999021001</td>
                </tr>
                <tr>
                  <td>Tempat, Tanggal Lahir</td>
                  <td>:</td>
                  <td>Lape-Sumbawa, 8 Oktober 1974</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>:</td>
                  <td>Laki - Laki</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>:</td>
                  <td>Islam</td>
                </tr>
                <tr>
                  <td>Pangkat / Golongan</td>
                  <td>:</td>
                  <td>Pembina Utama Muda / IV c</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>wirawanahmad@yahoo.co.id</td>
                </tr>
                <tr>
                  <td>Facebook</td>
                  <td>:</td>
                  <td>facebook.com/wirawan.ahmad.1</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={s.sosmed}>
            <Link href="https://www.facebook.com/wirawan.ahmad.1">
              <a>
                <div className={s.item}>
                  <FacebookIcon />
                </div>
              </a>
            </Link>
            <Link href="mailto:wirawanahmad@yahoo.co.id">
              <a>
                <div className={s.item}>
                  <EmailIcon />
                </div>
              </a>
            </Link>
            <Link href="https://instagram.com/wirawan8898?utm_medium=copy_link">
              <a>
                <div className={s.item}>
                  <InstagramIcon />
                </div>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div className={`${s.content} ${s.content_detail}`}>
        <div className={s.card}>
          <div className={s.title}>Riwayat Jabatan :</div>
          <ol>
            <li>
              Kepala Sub Bidang Penyusunan Rencana Kegiatan BAPPEDA Kab. Sumbawa
              (2003)
            </li>
            <li>
              Kepala Sub Bidang Pengkajian Strategis BAPPEDA Kab. Sumbawa (2005)
            </li>
            <li>
              Kasi Geologi dan Tata Lingkungan pada Dinas Pertambangan dan
              Energi Kab. Sumbawa (2005)
            </li>
            <li>
              Kasi Pendataan dan Penetapan Pajak Daerah DPKA Kab. Sumbawa (2008)
            </li>
            <li>
              Kepala Bidang Aset Daerah Dinas Pendapatan Keuangan dan Aset
              Daerah Kab. Sumbawa (2009)
            </li>
            <li>
              Kepala Bagian Humas dan Protokol Sekretariat Daerah Kab. Sumbawa
              (2011)
            </li>
            <li>
              Kepala Bagian Administrasi Perekonomian Pembangunan dan Layanan
              Pengadaan barang / jasa Sekretariat Daerah Kab. Sumbawa (2013)
            </li>
            <li>
              Kepala Kantor Pelayanan Perizinan Terpadu Kab. Sumbawa (2015)
            </li>
            <li>
              Kepala Badan Pengelola Keuangan dan Aset Daerah Kab. Sumbawa
              (2016-2019)
            </li>
            <li>
              Staf Ahli Gubernur Bidang Ekonomi, Keuangan & Infrastruktur (2020)
            </li>
          </ol>
        </div>
        <div className={s.nested_card}>
          <div className={s.card}>
            <div className={s.title}>Riwayat Pendidikan :</div>
            <ol>
              <li>SMAN 2 Mataram (1992)</li>
              <li>S1 Geografi Universitas Gadjah Mada (1998)</li>
              <li>S2 Teknik Sipil Universitas Gadjah Mada (2003)</li>
            </ol>
          </div>
          <div className={s.card}>
            <div className={s.title}>Riwayat Penghargaan :</div>
            <ol>
              <li>
                Pemateri Kuliah Umum Implementasi APBD Berkualitas Untuk
                Mempertkuat Pertumbuhan Ekonomi di Era Industri 4.0 Universitas
                Teknologi Sumbawa
              </li>
              <li>
                Narasumber Seminar Online dengan tema Pertumbuhan Sektor
                Pertanian dan Ancaman Resesi Ekonomi pleh Fakultas Pertanian
                Universitas Samawa
              </li>
            </ol>
          </div>
          <div className={s.card}>
            <div className={s.title}>Informasi LKHPN :</div>
            <div className={s.link}>
              <a href="/documents/kaban/LHKPN_435286_WIRAWAN.pdf">
                Klik Disini
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default index;
