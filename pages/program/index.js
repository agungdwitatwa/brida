import { React } from "react";
import Header from "../../components/Header/header";
import Navbar from "../../components/Navbar/navbar";
import Footer from "../../components/Footer/Footer";
import Banner from "../../components/Banner/Banner";
import Head from "next/head";
import s from "./layananPenelitian.module.css";
import BusinessCenterIcon from "@mui/icons-material/BusinessCenter";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Progam BRIDA NTB"}
        icon={<BusinessCenterIcon fontSize="large" />}
      />
      <div className={s.content}>
        <div className={s.item_container}>
          <div className={s.item}>
            <h2>1. Sunday Morning</h2>
            <p>
              Program ini memfasilitasi masyarakat sekitar kantor BRIDA NTB
              untuk dapat berolahraga, piknik atau mendapatkan konsultasi
              kesehatan gratis. Diadakan setiap hari Minggu, Pukul 06.00 - 09.00
              WITA di kawasan BRIDA NTB, BRIDA Sunday Morning juga turut
              memfasilitasi mitra atau tenan BRIDA dan Masyarakat yang ingin
              membuka gerai.{" "}
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pemanfaatan Riset dan
              Inovasi
            </p>
          </div>
          <div className={s.item}>
            <h2>2. Inkubasi Bisnis dan Kemitraan</h2>
            <p>
              Wadah bagi Masyarakat yang ingin mengembangkan bisnis dibawah
              naungan BRIDA NTB, program ini dilengkapi dengan kelas mentoring
              seperti kelas Digital Marketing, kelas Business Plan dan kelas
              Ekspor.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Kemitraan dan Inkubasi
              Bisnis
            </p>
          </div>
        </div>
        <div className={s.item_container}>
          <div className={s.item}>
            <h2>3. Program Beasiswa Luar dan Dalam Negeri</h2>
            <p>
              Memberikan kesempatan bagi putra putri NTB untuk dapat mengeyam
              pendidikan lebih tinggi sebagai bentuk Investasi Sumber Daya
              Manusia untuk NTB di masa depan. Program Beasiswa tersedia dengan
              berbagai skema baik tujuan dalam negeri maupun luar negeri.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pengembangan Sumber Daya
              Ilmu Pengetahuan dan Teknologi
            </p>
          </div>
          <div className={s.item}>
            <h2>4. Penelitian dan Pengembangan Mesin Inovasi</h2>
            <p>
              Program dibawah Bidang Penelitian, Pengembangan Inovasi dan
              Teknologi yang telah berhasil memfasilitasi lebih dari 100 unit
              Prototipe untuk dapat menjadi produk yang sesuai standar dan
              kebutuhan di masyarakat.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Penelitian, Pengembangan
              Inovasi dan Teknologi
            </p>
          </div>
        </div>
        <div className={s.item_container}>
          <div className={s.item}>
            <h2>5. Uji Kompetensi Sertifikasi</h2>
            <p>
              Memberikan pelatihan dan sertifikasi BNSP kepada masyarakat secara
              offline, untuk dapat menghasilkan tenaga kerja yang diakui
              kemampuannya telah kompeten sehingga dapat memberikan jaminan
              kualitas dari jasa yang dihasilkan.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pengembangan Sumber Daya
              Ilmu Pengetahuan dan Teknologi
            </p>
          </div>
          <div className={s.item}>
            <h2>6. BRIDA School Academy</h2>
            <p>
              Sebagai wadah bagai Mahasiswa yang melaksakan magang di BRIDA NTB
              untuk dapat mempelajari hal baru dan mendapat nilai tambah melalui
              kelas Programmer, saat ini BRIDA School Academy telah tersedia
              bagi masyarakat luas secara gratis.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pengembangan Sumber Daya
              Ilmu Pengetahuan dan Teknologi
            </p>
          </div>
        </div>
        <div className={s.item_container}>
          <div className={s.item}>
            <h2>7. Rumah Bahasa</h2>
            <p>
              Memberikan kesempatan bagi masyarakat untuk mengikuti kursus
              intensif Bahasa Inggris hingga mendapatkan sertifikar TOELF/IELTS
              resmi secara gratis.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pengembangan Sumber Daya
              Ilmu Pengetahuan dan Teknologi
            </p>
          </div>
          <div className={s.item}>
            <h2>8. Eduwisata</h2>
            <p>
              BRIDA NTB sebagai etalase pengembangan inovasi dan teknologi
              menjadi tempat belajar bagi siswa dan sekolah melalui program
              Eduwisata .
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pemanfaatan Riset dan
              Inovasi{" "}
            </p>
          </div>
        </div>
        <div className={s.item_container}>
          <div className={s.item}>
            <h2>9. Desiminasi Hasil Riset dan Inovasi</h2>
            <p>
              Hasil Riset dan Inovasi yang telah dikembangkan di BRIDA NTB di
              Desiminasi ke Masyarakat atau Organisasi terkait
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pemanfaatan Riset dan
              Inovasi{" "}
            </p>
          </div>
          <div className={s.item}>
            <h2>10. Uji Standarisasi Produk</h2>
            <p>
              Setelah Mesin Inovasi masyarakat dikembangkan dan disempurnakan,
              BRIDA NTB juga memfasilitasi mesin inovasi tersebut dengan Uji
              Standarisasi agar dapat dikomersilkan dan dinyatakan aman untuk
              digunakan masyarakat.
            </p>
            <p>
              Program ini diselenggarakan oleh Bidang Pengembangan Sumber Daya
              Ilmu Pengetahuan dan Teknologi
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
