import React from "react";
import s from "./berita.module.css";
import Header from "../../components/Header/header";
import Navbar from "../../components/Navbar/navbar";
import Banner from "../../components/Banner/Banner";
import ArticleIcon from "@mui/icons-material/Article";
import { createClient } from "contentful";
import InfiniteScroll from "react-infinite-scroll-component";
import { useEffect, useState } from "react";
import Card from "../../components/Card/Berita";
import Footer from "../../components/Footer/Footer";
import Head from "next/head";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
    accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
  });
  const res = await client.getEntries({
    content_type: "berita",
    limit: 4,
    select: "fields",
    order: "-fields.tanggalBerita",
  });

  return {
    props: {
      data: res.items,
      data_total: res.total,
      spaceID: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
      token: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
    },
  };
}

function BeritaComp({ data, data_total, spaceID, token }) {
  const [berita, setBerita] = useState(data);
  const [hasMore, setHasMore] = useState(true);

  const getMoreBerita = async () => {
    const clnt = createClient({
      space: spaceID,
      accessToken: token,
    });
    const res = await clnt.getEntries({
      content_type: "berita",
      skip: berita.length,
      limit: 4,
      select: "fields",
      order: "-fields.tanggalBerita",
    });
    setBerita((berita) => [...berita, ...res.items]);
  };

  useEffect(() => {
    setHasMore(data_total > berita.length ? true : false);
  }, [berita]);
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Berita Seputar BRIDA Provinsi NTB"}
        icon={<ArticleIcon fontSize="large" />}
      />
      <InfiniteScroll
        dataLength={berita.length}
        next={getMoreBerita}
        hasMore={hasMore}
        loader={<h1>Loading . . . </h1>}
        className={s.berita_container}
      >
        {berita.map((item) => (
          <div key={item.sys.id} className={s.berita_container_item}>
            <Card
              judulBerita={item.fields.judulBerita}
              cover={item.fields.thumbnail.fields.file.url}
              width={item.fields.thumbnail.fields.file.details.image.width}
              link={item.fields.slug}
              className={s.berita_item}
            />
          </div>
        ))}
      </InfiniteScroll>
      <Footer />
    </div>
  );
}

export default BeritaComp;
