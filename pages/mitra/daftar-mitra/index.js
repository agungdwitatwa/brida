import React from "react";
import s from "./daftarMitra.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import Footer from "../../../components/Footer/Footer";
import Table from "../../../components/Table/Table";
import GroupsIcon from "@mui/icons-material/Groups";
import Head from "next/head";

function Index() {
  const columns = [
    { id: "number", label: "No." },
    { id: "namaPerusahaan", label: "Nama Perusahaan", minWidth: 170 },
    { id: "namaBrand", label: "Nama Brand", minWidth: 100 },
    {
      id: "namaPemilik",
      label: "Nama Pemilik",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaPerusahaan: "OLAT MARAS POWER",
      namaBrand: "NgebUTS",
      namaPemilik: "AHMAD JAYA",
    },
    {
      number: "2",
      namaPerusahaan: "CV. LOMBOK E-BIKE BUILDER",
      namaBrand: "LE-BUI",
      namaPemilik: "GEDE SUKARMA DIJAYA",
    },
    {
      number: "3",
      namaPerusahaan: "CV. RONAS INDONESIA",
      namaBrand: "RONAS CLEAN",
      namaPemilik: "MUH. ZULHAM MARDIANSYAH",
    },
    {
      number: "4",
      namaPerusahaan: "DAPUR TAMI",
      namaBrand: "DAPUR TAMI",
      namaPemilik: "RATNA UTAMI",
    },
    {
      number: "5",
      namaPerusahaan: "CV. TRI UTAMI JAYA",
      namaBrand: "KIDOM ( Kilo Dompu)",
      namaPemilik: "NASRIN, S.Adm.",
    },
    {
      number: "6",
      namaPerusahaan: "Coming Soong",
      namaBrand: "Coming Soon",
      namaPemilik: "Coming Soon",
    },
  ];
  return (
    <div className={s.container}>
      <Head>
        <title>
          Daftar Mitra | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa
          Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner name={"Daftar Mitra"} icon={<GroupsIcon fontSize="large" />} />
      <div className={s.content}>
        <iframe
          src="/documents/NEW KATALOG PRODUK IKM MITRA BRIDA NTB 2022.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"700px"}
          allowFullScreen
        ></iframe>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
