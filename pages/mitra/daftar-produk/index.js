import React from "react";
import s from "./daftarProduk.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Banner from "../../../components/Banner/Banner";
import GroupsIcon from "@mui/icons-material/Groups";
import Head from "next/head";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner name={"Produk Mitra"} icon={<GroupsIcon fontSize="large" />} />
      <div className={s.content}>
        <iframe
          src="/documents/NEW KATALOG PRODUK IKM MITRA BRIDA NTB 2022.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"700px"}
          allowFullScreen
        ></iframe>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
