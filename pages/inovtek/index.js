import React from "react";
import s from "./style.module.css";
import Header from "../../components/Header/header";
import Navbar from "../../components/Navbar/navbar";
import Footer from "../../components/Footer/Footer";
import Head from "next/head";
import Countdown from "react-countdown";
import Link from "next/link";
import { createClient } from "contentful";
import Button from "@mui/material/Button";
import EventNoteTwoToneIcon from "@mui/icons-material/EventNoteTwoTone";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
    accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
  });
  const berita = await client.getEntries({
    content_type: "berita",
    select: "fields",
    limit: 4,
    order: "-fields.tanggalBerita",
    "metadata.tags.sys.id[all]": "inovtek",
  });

  return {
    props: {
      berita: berita.items,
    },
  };
}

function Index({ berita }) {
  // const renderer = ({ days, hours, minutes, seconds }) => {
  //   // Render a countdown
  //   return (
  //     // <span>
  //     // {days} Hari &nbsp; : &nbsp; {hours} Jam &nbsp; : &nbsp; {minutes} Menit
  //     // &nbsp; : &nbsp;
  //     // {seconds} Detik
  //     // </span>
  //     <div className={s.countdown_container}>
  //       <div className={s.item}>
  //         <div>{days}</div>
  //         <div>Hari</div>
  //       </div>
  //       <div className={s.item}>
  //         <div>{hours}</div>
  //         <div>Jam</div>
  //       </div>
  //       <div className={s.item}>
  //         <div>{minutes}</div>
  //         <div>Menit</div>
  //       </div>
  //       <div className={s.item}>
  //         <div>{seconds}</div>
  //         <div>Detik</div>
  //       </div>
  //       {/* {days} Hari &nbsp; : &nbsp; {hours} Jam &nbsp; : &nbsp; {minutes} Menit
  //       &nbsp; : &nbsp;
  //       {seconds} Detik */}
  //     </div>
  //   );
  // };
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <div className={s.imageTitle}>
          <img src="/image/titleInovtek.png" />
        </div>
        <div className={s.countdown}>
          <iframe
            width="100%"
            height="400"
            src="https://www.youtube.com/embed/_H99-pvyyS4"
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className={s.iframe}
          ></iframe>
        </div>
      </div>
      <div className={s.bannerImage}>
        <img
          src="/image/bannerInov.png"
          width={"100%"}
          // height={"500px"}
        />
      </div>

      <div className={s.location}>
        <Link href={"/inovtek/lokasi"}>
          <a>KLIK DISINI UNTUK MELIHAT LOKASI DAN VENUE ACARA</a>
        </Link>
      </div>
      <div className={s.circleMenu}>
        <div className={s.item}>
          <Link href={"/inovtek/hiburan"}>
            <a>
              <div className={s.icon}>
                <img src="/svg/hiburan.svg" />
              </div>
              <div className={s.text}>Hiburan & Bazar</div>
            </a>
          </Link>
        </div>
        <div className={s.item}>
          <Link href={"/inovtek/pameran"}>
            <a>
              <div className={s.icon}>
                <img src="/svg/pameran.svg" />
              </div>
              <div className={s.text}>Pameran IPTEK & UMKM</div>
            </a>
          </Link>
        </div>
        <div className={s.item}>
          <Link href={"/inovtek/seminar"}>
            <a>
              <div className={s.icon}>
                <img src="/svg/seminar.svg" />
              </div>
              <div className={s.text}>Seminar</div>
            </a>
          </Link>
        </div>
        <div className={s.item}>
          <Link href={"/inovtek/sponsor"}>
            <a>
              <div className={s.icon}>
                <img src="/svg/sponsor.svg" />
              </div>
              <div className={s.text}>Sponsor</div>
            </a>
          </Link>
        </div>
      </div>
      <div className={s.berita}>
        <div className={s.title}>
          <div>Berita Utama</div>
          <div className={s.line}></div>
        </div>
        <div className={s.listBerita}>
          {berita.map((berita, index) => (
            <div className={s.berita_item} key={index}>
              <div>
                <img src={`https:${berita.fields.thumbnail.fields.file.url}`} />
              </div>
              <div className={s.desc_berita}>
                <div>
                  <div>{berita.fields.judulBerita}</div>
                  <div>{berita.fields.preview}</div>
                  <div className={s.tanggal}>
                    <EventNoteTwoToneIcon />{" "}
                    {berita.fields.tanggalBerita.split("-").reverse().join("-")}
                  </div>
                </div>
                <Link href={`/berita/${berita.fields.slug}`}>
                  <a>Selengkapnya</a>
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
