import React from "react";
import s from "./style.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import Link from "next/link";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <Link href={"/inovtek"}>
          <a>
            <div className={s.imageTitle}>
              <img src="/image/titleInovtek.png" />
            </div>
          </a>
        </Link>
      </div>
      <div className={s.header}>
        <div className={s.title}>Acara Hiburan dan Pasar Murah</div>
        <div className={s.line}></div>
      </div>
      <div className={s.content}>
        <div>
          <img src="/image/inovtek/hiburan.png" />
        </div>
        <div className={s.desc}>
          <div>Dapatkan beragam produk di Pasar Murah Dinas Perdagangan!</div>
          <div>
            <div>Pemutaran Film Putri Mandalika </div>
            <div>Jumat 18 Maret 2022</div>
            <div>19.00 - 22.00 WITA</div>
          </div>
        </div>
      </div>
      <div className={`${s.content} ${s.reverse}`}>
        <div>
          <img src="/image/inovtek/presean.png" />
        </div>
        <div className={s.desc}>
          <div>
            Saksikan keindahan budaya NTB melalui penampilan seni tradisional
            Peresean dan Gendang Beleq
          </div>
        </div>
      </div>
      <div className={s.line}></div>
      <Footer />
    </div>
  );
}

export default Index;
