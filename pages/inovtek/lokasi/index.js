import React from "react";
import s from "./style.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import Link from "next/link";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <Link href={"/inovtek"}>
          <a>
            <div className={s.imageTitle}>
              <img src="/image/titleInovtek.png" />
            </div>
          </a>
        </Link>
      </div>
      <div className={s.header}>
        <div className={s.title}>Venue Acara Inovtek Expo</div>
        <div className={s.line}></div>
      </div>
      <div>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.4946360398076!2d116.1130116150531!3d-8.644415993788227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdbf89c5193d35%3A0x114a8b208a78c166!2sBadan%20Riset%20dan%20Inovasi%20Daerah%20(BRIDA)%20NTB!5e0!3m2!1sen!2sid!4v1646223564668!5m2!1sen!2sid"
          width="100%"
          height="400"
          //   style="border:0;"
          style={{ border: 0 }}
          allowFullScreen=""
          loading="lazy"
          className={s.maps}
        ></iframe>
      </div>

      <div className={`${s.header} ${s.header_plus}`}>
        <div className={s.title}>Lokasi Acara Inovtek Expo</div>
        <div className={s.line}></div>
      </div>
      <div className={s.venue}>
        <img src="/image/inovtek/venue1.png" width="100%" />
      </div>
      <div className={s.venue}>
        <img src="/image/inovtek/venue2.png" width="100%" />
      </div>
      <div className={s.venue}>
        <img src="/image/inovtek/venue3.png" width="100%" />
      </div>
      <Footer />
    </div>
  );
}

export default Index;
