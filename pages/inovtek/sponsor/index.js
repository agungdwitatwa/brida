import React from "react";
import s from "./style.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import Countdown from "react-countdown";
import Link from "next/link";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <Link href={"/inovtek"}>
          <a>
            <div className={s.imageTitle}>
              <img src="/image/titleInovtek.png" />
            </div>
          </a>
        </Link>
      </div>
      <div className={s.content}>
        <div className={s.title}>SPONSORED BY</div>
        <div className={s.sponsor}>
          <div className={s.double}>
            <div className={s.image}>
              <Link href="https://www.bankntbsyariah.co.id">
                <a>
                  <img src="/image/inovtek/ntbsyariah.png" />
                </a>
              </Link>
            </div>
            <div className={`${s.image} ${s.gne}`}>
              <Link href="https://www.gne.co.id">
                <a>
                  <img src="/image/inovtek/gne.png" />
                </a>
              </Link>
            </div>
            <div className={s.image}>
              <Link href="https://www.telkom.co.id/sites">
                <a>
                  <img src="/image/inovtek/telkom.png" />
                </a>
              </Link>
            </div>
          </div>
          <div className={s.double}>
            <div className={s.image}>
              <Link href="https://www.bpjsketenagakerjaan.go.id/">
                <a>
                  <img src="/image/inovtek/bpjs.png" />
                </a>
              </Link>
            </div>
            <div className={s.image}>
              <Link href="https://instagram.com/globalwakafcorporation?utm_medium=copy_link">
                <a>
                  <img src="/image/inovtek/wakaf.png" />
                </a>
              </Link>
            </div>
            <div className={s.image}>
              <Link href="https://instagram.com/grabid?utm_medium=copy_link">
                <a>
                  <img src="/image/inovtek/grab.png" />
                </a>
              </Link>
            </div>
          </div>
          <div className={s.double}>
            <div className={s.image}>
              <Link href="https://www.inovasigroup.com/about-us/">
                <a>
                  <img src="/image/inovtek/inovasi.png" />
                </a>
              </Link>
            </div>
            <div className={s.image}>
              <Link href="https://solindoconvex.com/index.html">
                <a>
                  <img src="/image/inovtek/solindo.png" />
                </a>
              </Link>
            </div>
            <div className={s.image}>
              <Link href="https://mataramkota.bnn.go.id/">
                <a>
                  <img src="/image/inovtek/bnn.png" />
                </a>
              </Link>
            </div>
          </div>
          <div className={s.singleImage}>
            <Link href="https://utomodeck.com/">
              <a>
                <img src="/image/inovtek/otomodeck.png" />
              </a>
            </Link>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}

export default Index;
