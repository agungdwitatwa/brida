import React from "react";
import s from "./style.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import Countdown from "react-countdown";
import Link from "next/link";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <Link href={"/inovtek"}>
          <a>
            <div className={s.imageTitle}>
              <img src="/image/titleInovtek.png" />
            </div>
          </a>
        </Link>
      </div>
      <div className={s.header}>
        <div className={s.title}>Pameran IPTEK dan UMKM</div>
        <div className={s.line}></div>
      </div>
      <div className={s.content}>
        BRIDA NTB mengadakan Pameran IPTEK dan UMKM akan ada 45 jenama lokal
        keren yang terkurasi dari UMKM dan pelaku inovasi di NTB lho! Di sana
        akan ada produk teknologi dan olahan pangan yang menarik, dan juga akan
        ada produk-produk makanan dari masyarakat Desa sekitar yang bisa
        dinikmati sambil menikmati hiburan yang ada.
      </div>
      <div className={s.image}>
        <img src="/image/inovtek/pamage2.png" />
        <img src="/image/inovtek/pamage.jpg" />
      </div>
      <div className={s.content}>
        Jangan lupa untuk tetap mematuhi Protokol Kesehatan dengan menjaga
        jarak, menggunakan Masker dan Hand Sanitizer, pastikan scan Peduli
        Lindungi ya. Gratis Masuk untuk semua pengunjung! Ditunggu kedatangannya
        yaa Sobat Inovasi!
      </div>
      <div className={s.temp}></div>
      <Footer />
    </div>
  );
}

export default Index;
