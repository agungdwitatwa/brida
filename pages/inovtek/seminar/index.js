import React from "react";
import s from "./style.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Footer from "../../../components/Footer/Footer";
import Head from "next/head";
import Countdown from "react-countdown";
import Link from "next/link";
import Image from "next/image";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar style={{ backgroundColor: "#371C53 !important" }} />
      <div className={s.banner}>
        <Link href={"/inovtek"}>
          <a>
            <div className={s.imageTitle}>
              <img src="/image/titleInovtek.png" />
            </div>
          </a>
        </Link>
      </div>
      <div className={s.header}>
        <div className={s.title}>Seminar Inovtek</div>
        <div className={s.line}></div>
      </div>
      <div className={s.content}>
        <div>
          Ikuti kisah sukses para pelaku riset dan inovasi di Nusa Tenggara
          Barat!
        </div>
        <div className={s.image}>
          <img src="/image/inovtek/seminar.png" />
        </div>
        <div className={`${s.image} ${s.seminar}`}>
          <img src="/image/inovtek/scholarship.jpg" />
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Index;
