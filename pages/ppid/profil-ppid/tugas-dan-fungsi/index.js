import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./tugasFungsi.module.css";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Tugas dan Fungsi - PPID BRIDA Provinsi NTB"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{
          color: "#10468f",
          backgroundImage: "none",
        }}
      />
      <div className={s.content}>
        <div className={s.text}>
          Berdasarkan Peraturan Gubernur No. 24 Tahun 2018 tentang Tata Kerja
          Pejabat Pengelola Informasi dan Data Pemerintah Provinsi NTB, PPID
          BRIDA NTB diharapkan dapat memberikan pelayanan informasi yang
          berkualitas kepada publik guna mewujudkan penyelenggaraan pemerintah
          yang baik, terbuka, efektif dan efisien akuntabel serta dapat
          dipertangggung jawabkan.
        </div>
        <div className={s.title}>
          Tugas PPID menurut Pergub NTB Nomor 24 Tahun 2018 pasal 14 ayat (2)
        </div>
        <div className={s.text}>
          <ol>
            <li>
              Melaksanakan kebijakan penyelenggaraan pelayanan informasi publik
            </li>
            <li>
              Mendokumentasikan, memverifikasi, menyusun, menyimpan dan
              mengelola bahan-bahan Informasi publik
            </li>
            <li>
              Mendokumentasikan, Menyimpan menyediakan, dan memberi pelayanan
              informasi publik
            </li>
            <li>
              Mengajukan permohonan konsultasi uji konsekuensi untuk
              pengecualian informasi kepada PPID Provinsi
            </li>
            <li>
              Konsultasi upaya penyelesaian sengketa informasi publik kepada
              PPID Provinsi
            </li>
            <li>Melaksanakan tugas-tugas yang diberikan oleh PPID Provinsi</li>
          </ol>
        </div>
        <div className={s.title}>
          Fungsi PPID Pembantu menurut Pergub NTB Nomor 24 Tahun 2018 pasal 14
          ayat (2)
        </div>
        <div className={s.text}>
          <ol>
            <li>
              Pendokumentasian, pengelolaan, penyimpanan, pengklasifikasian
              bahan-bahan Informasi publik;
            </li>
            <li>Penyimpanan, pengelolaan, dokumen informasi publik; dan</li>
            <li>Pelayanan informasi publik.</li>
          </ol>
        </div>
      </div>
      <div className={s.content}>
        <h1>SK Penetapan PPID BRIDA NTB</h1>
        <iframe
          src="/documents/ppid/SK PPID BRIDA NTB 2022.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
      </div>

      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
