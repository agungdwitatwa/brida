import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./visiMisi.module.css";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Visi Misi - PPID BRIDA Provinsi NTB"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content_wrapper}>
        <div className={s.content}>
          <div className={s.title}>Visi PPID :</div>
          <div className={s.text}>
            Terwujudnya pelayanan informasi yang transparan dan akuntabel untuk
            memenuhi hak pemohon informasi sesuai dengan ketentuan peraturan
            perundang-undangan yang berlaku.
          </div>
        </div>
        <div className={s.content}>
          <div className={s.title}>Misi PPID :</div>
          <div className={s.text}>
            <ol>
              <li>
                Meningkatkan pengelolaan dan pelayanan informasi yang
                berkualitas, benar dan bertanggung jawab.
              </li>
              <li>
                Membangun dan mengembangkan sistem penyediaan dan layanan
                informasi.
              </li>
              <li>
                Meningkatkan dan mengembangkan kompetensi dan kualitas SDM dalam
                bidang pelayanan informasi.
              </li>
              <li>
                Mewujudkan keterbukaan informasi Pemerintah Provinsi NTB dengan
                proses yang cepat, tepat, mudah dan sederhana.
              </li>
            </ol>
          </div>
        </div>
      </div>

      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
