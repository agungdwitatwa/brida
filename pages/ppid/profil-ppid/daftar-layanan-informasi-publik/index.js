import React from "react";

function index() {
  return (
    <div style={{ position: "absolute", width: "100vw", height: "100vh" }}>
      <iframe
        src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR-cgCEYAnF1YSomKR4UuyQO-kDmZnOWcC0Ps27P40v4DTrRuQq3JUHJNN30Zr_VB0AHvqzasDGPRmw/pubhtml?gid=704125103&amp;single=true&amp;widget=true&amp;headers=false"
        width="100%"
        height="100%"
      ></iframe>
    </div>
  );
}

export default index;
