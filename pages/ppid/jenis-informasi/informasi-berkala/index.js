import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./informasiBerkala.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaInformasi", label: "Nama Informasi", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaInformasi: "Agenda Kerja Pimpinan",
      link: "/ppid/jenis-informasi/agenda-harian-kepala-badan",
    },
    {
      number: "2",
      namaInformasi: "Capaian Kinerja Beasiswa NTB dan Rumah Bahasa",
      link: "https://drive.google.com/file/d/1WLRFFSgfdzNiM9hzvs60VLgXudFOzmyM/view?usp=sharing",
    },

    {
      number: "3",
      namaInformasi: "Daftar Penerima Beasiswa NTB dan Rumah Bahasa",
      link: "https://drive.google.com/drive/folders/1TATtOjkkzQbI5f6bjHE5rDeSE0Zwxn2u?usp=sharing",
    },
    {
      number: "4",
      namaInformasi: "Daftar Ijin Penelitian masyarakat sesuai aplikasi e-LIP",
      link: "https://drive.google.com/file/d/1ezokSw_IVi2XL5Hf_U2bnbycBaQ7paaQ/view?usp=sharing",
    },
    {
      number: "5",
      namaInformasi:
        "Daftar Indeks Inovasi Daerah ( Government Award) Provinsi NTB",
      link: "/documents/ppid/11. PDF_PROGR_REP_20210918.pdf",
    },
    {
      number: "6",
      namaInformasi: "Daftar Indeks Pengelolaan Keuangan Daerah Provinsi NTB",
      link: "#",
    },
    {
      number: "7",
      namaInformasi: "Daftar Inkubasi Startup",
      link: "/documents/ppid/Daftar Komunitas Startup BRIDA NTB 2022.pdf",
    },
    {
      number: "8",
      namaInformasi: "Daftar Kunjungan Eduwisata",
      link: "/documents/ppid/Laporan Kunjungan Edu Wisata BRIDA NTB- Triwulan 2022.pdf",
    },
    {
      number: "9",
      namaInformasi: "Daftar IKM Mitra & Mesin Prototipe",
      link: "/documents/NEW KATALOG PRODUK IKM MITRA BRIDA NTB 2022.pdf",
    },
    {
      number: "10",
      namaInformasi: "Daftar Penelitian yang dilakukan BRIDA",
      link: "#",
    },
    {
      number: "11",
      namaInformasi: "Daftar riset/kajian yang difasilitasi BRIDA",
      link: "https://drive.google.com/file/d/15CQvFcUsyGbANQYmHDn8pFVoaLu-dWeP/view?usp=sharing",
    },
    {
      number: "12",
      namaInformasi: "Daftar Sertifikasi Kompetensi/Profesi",
      link: "/documents/ppid/Daftar Peserta Sertifikasi Profesi 2022.pdf",
    },
    {
      number: "13",
      namaInformasi: "Daftar Sertifikasi Produk",
      link: "#",
    },
    {
      number: "14",
      namaInformasi: "Daftar UMKM Mitra (tenant)",
      link: "#",
    },
    {
      number: "15",
      namaInformasi: "Daftar/Agenda Surat keluar masuk",
      link: "#",
    },
    {
      number: "16",
      namaInformasi: "Dokumentasi Kegiatan",
      link: "/berita",
    },
    {
      number: "17",
      namaInformasi:
        "Laporan Keuangan; LRA, Neraca, CALK, Asset dan inventaris",
      link: "/documents/ppid/LAPORAN REALISASI BRIDA NTB TRIWULAN 1 (JAN-MAR).pdf",
    },
    {
      number: "18",
      namaInformasi: "Laporan Kinerja BRIDA, semester dan tahunan",
      link: "/documents/ppid/RKPD Triwulan II 2022 - BRIDA.pdf",
    },
    {
      number: "19",
      namaInformasi: "Laporan Layanan Informasi Publik",
      link: "#",
    },
    {
      number: "20",
      namaInformasi: "Renja BRIDA",
      link: "/documents/Renja BRIDA NTB.pdf",
    },
    {
      number: "21",
      namaInformasi: "Renstra BRIDA",
      link: "/documents/Renstra BRIDA NTB.pdf",
    },
    {
      number: "22",
      namaInformasi: "RAK (Rincian Anggaran Kas) BRIDA",
      link: "/documents/RAK SKPD - Badan Riset dan Inovasi Daerah Provinsi Nusa Tenggara Barat.pdf",
    },
    {
      number: "23",
      namaInformasi: "DPPA (Dokumen Pelaksanaan Pergeseran Anggaran) BRIDA",
      link: "/documents/DPPA-BELANJA BRIDA - 2022.pdf",
    },
    {
      number: "24",
      namaInformasi: "Sistem Informasi Rencanan Umum Pengadaan",
      link: "/documents/Sistem Informasi Rencana Umum Pengadaan BRIDA 2022.pdf",
    },
    {
      number: "25",
      namaInformasi: "Naskah Akademik BRIDA",
      link: "/documents/Naskah Akademik BRIDA.pdf",
    },
    {
      number: "26",
      namaInformasi:
        "Sistem Informasi Rencanan Umum Pengadaan (23 September 2022)",
      link: "https://drive.google.com/file/d/1Hpn3pkSrV_wZAkT3kHxWx5opJq1yMmHS/view?usp=sharing",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Informasi Berkala"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka Link"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
