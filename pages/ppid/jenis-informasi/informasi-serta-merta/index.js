import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./informasiSerta.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaInformasi", label: "Nama Informasi", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaInformasi: "Petunjuk Lokasi Ruang Rapat",
      link: "#",
    },
    {
      number: "2",
      namaInformasi: "Jalur evakuasi bencana (Gedung BRIDA)",
      link: "https://drive.google.com/drive/u/3/folders/1whaPDnMqeNK0R2Usf23bGEgdiHpG1M0f",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Informasi Serta Merta"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka Link"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
