import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./agendaHarian.module.css";
import { createClient } from "contentful";
import Table from "../../../../components/Table/Table";

export async function getStaticProps() {
  const client = createClient({
    space: "yhv40pnlg70o",
    accessToken: "ho3n-fblnmKTj8z8e61eQ6L1L4ORhzKKo-S5IuXKvVk",
  });
  const res = await client.getEntries({
    content_type: "agendaKaban",
  });

  return {
    props: {
      agenda: res.items,
    },
  };
}

function Index({ agenda }) {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaKegiatan", label: "Nama Kegiatan", minWidth: 100 },
    { id: "tanggalKegiatan", label: "Tanggal Kegiatan", minWidth: 100 },
    { id: "tempatKegiatan", label: "Tempat Kegiatan", minWidth: 100 },
  ];

  const rows = [];

  agenda.map((agenda, index) => {
    rows.push({
      number: index + 1,
      namaKegiatan: agenda.fields.namaKegiatan,
      tanggalKegiatan: agenda.fields.tanggalKegiatan
        .split("-")
        .reverse()
        .join("-"),
      tempatKegiatan: agenda.fields.tempatKegiatan,
    });
  });

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Agenda Harian Kepala Badan"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
