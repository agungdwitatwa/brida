import React from "react";
import Header from "../../../../../components/Header/header";
import NavbarPPID from "../../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./setiapSaat.module.css";
import Table from "../../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaInformasi", label: "Nama Informasi", minWidth: 100 },
    { id: "jenisInformasi", label: "Jenis Informasi" },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaInformasi:
        "Koordinasi Perangkat Daerah Terkait Pengisian Indeks Pengelolaan Keuangan Daerah",
      jenisInformasi: "Berita",
      link: "https://www.instagram.com/p/CedMSbfBjHw/",
    },
    {
      number: "2",
      namaInformasi:
        "Rapat Koordinasi Terbatas Dengan Beberapa Perangkat Daerah Terkait Pengukuran IPKD",
      jenisInformasi: "Berita",
      link: "https://www.instagram.com/p/Ce0ZBGWhxTr/?utm_source=ig_web_copy_link",
    },
    {
      number: "3",
      namaInformasi:
        "NTB Siap! Berupaya Mencapai Predikat Terbaik dengan Memenuhi 6 dimensi IPKD",
      jenisInformasi: "Berita",
      link: "https://brida.ntbprov.go.id/berita/ntb-siap-berupaya-mencapai-predikat-terbaik-dengan-memenuhi-6-dimensi-ipkd",
    },
    {
      number: "4",
      namaInformasi: "Time Scehdule IPKD",
      jenisInformasi: "Dokumen",
      link: "/documents/ppid/TIME_SECHEDULE_IPKD_Th.2022__.pdf",
    },
    {
      number: "5",
      namaInformasi: "Progres IPKD Kabupaten/Kota (28 Juni 2022)",
      jenisInformasi: "Dokumen",
      link: "/documents/ppid/PROGRESS IPKD KABUPATEN KOTA NTB 28 JUNI 2022.pdf",
    },
    {
      number: "6",
      namaInformasi: "Progres IPKD Kabupaten/Kota (12 Juli 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/file/d/1M_QrWKjfBHTistPSi5KWIE4r1CAtZvHk/view?usp=sharing",
    },
    {
      number: "7",
      namaInformasi: "Progres IPKD Kabupaten/Kota (25 Juli 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/drive/folders/1RdbN6HEl6JQnU63voB9xQOt2xILXKll3?usp=sharing",
    },
    {
      number: "8",
      namaInformasi: "Progres IPKD Provinsi NTB (2 Agustus 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/file/d/1vy7XFPmF426uZZfXcw0XrOywNdpn74gF/view?usp=sharing",
    },
    {
      number: "9",
      namaInformasi: "Progres IPKD Provinsi NTB (11 Agustus 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/file/d/1JkwkhX4jQHidKO7nIg4brWR-6I5X0NE4/view?usp=sharing",
    },
    {
      number: "10",
      namaInformasi: "Progres IPKD Provinsi NTB (1 September 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/file/d/1eBM5_vhoxrUhlEx3H-zlRK2i9BaiBgSB/view?usp=sharing",
    },
    {
      number: "11",
      namaInformasi: "Progres IPKD Provinsi NTB (9 September 2022)",
      jenisInformasi: "Dokumen",
      link: "https://drive.google.com/drive/folders/1-gjLKipZyGc22dQLQHHISfZb0xyRVYJt?usp=sharing",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Informasi Indeks Pengelolaan Keuangan Daerah"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka Link"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
