import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./setiapSaat.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaInformasi", label: "Nama Informasi", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaInformasi: "Data perbendaharaan, asset, inventaris",
      link: "/documents/ppid/Data Inventaris dan Barang Milik Daerah BRIDA NTB-dikonversi.pdf",
    },
    {
      number: "2",
      namaInformasi: "Informasi Inovasi OPD dan Masyarakat",
      link: "#",
    },
    {
      number: "3",
      namaInformasi:
        "Informasi Indeks Pengelolaan Keuangan Daerah Provinsi NTB",
      link: "/ppid/jenis-informasi/informasi-setiap-saat/informasi-indeks-pengelolaan-keuangan-daerah",
    },
    {
      number: "4",
      namaInformasi: "Informasi kunjungan eduwisata",
      link: "#",
    },
    {
      number: "5",
      namaInformasi: "Informasi Layanan Beasiswa",
      link: "https://lppntb.com/program/beasiswa-ntb",
    },
    {
      number: "6",
      namaInformasi: "Informasi Mesin Prototype",
      link: "/mitra/daftar-produk",
    },
    {
      number: "7",
      namaInformasi: "Informasi pelaksanaan layanan ijin penelitian",
      link: "/documents/ppid/REKAP IJIN PENELITIAN SD FEB 2022.pdf",
    },
    {
      number: "8",
      namaInformasi: "Informasi riset/kajian yang difasilitasi BRIDA",
      link: "/documents/ppid/Rekap Kajian Litbang.pdf",
    },
    {
      number: "9",
      namaInformasi: "Informasi Sertifikasi",
      link: "#",
    },
    {
      number: "10",
      namaInformasi: "Informasi Startup",
      link: "#",
    },
    {
      number: "11",
      namaInformasi: "Informasi UMKM Mitra (tenan)",
      link: "#",
    },
    {
      number: "12",
      namaInformasi:
        "Informasi, data detail tentang organisasi, administrasi kepagawaian, keuangan",
      link: "#",
    },
    {
      number: "13",
      namaInformasi:
        "Laporan Masyarakat; Jumlah Jenis dan gambaran umum pelanggaran yang dilaporkan",
      link: "#",
    },
    {
      number: "14",
      namaInformasi:
        "Laporan Pengawasan Internal dan penindakannya; Jumlah Jenis dan gambaran umum pelanggaran yang ditemukan",
      link: "#",
    },
    {
      number: "15",
      namaInformasi: "Peraturan / Keputusan dan kebijakan BRIDA",
      link: "#",
    },
    {
      number: "16",
      namaInformasi: "Surat menyurat pejabat BRIDA terkait tupoksi",
      link: "#",
    },
    {
      number: "17",
      namaInformasi: "Tabel Daftar Informasi Publik",
      link: "/documents/DIP_BRIDA_2022.pdf",
    },
    {
      number: "18",
      namaInformasi:
        "Informasi pelaksanaan kegiatan Layanan informasi Publik; Sarana prasarana, SDM, Anggaran, Laporan Layanan",
      link: "#",
    },
    {
      number: "19",
      namaInformasi: "Daftar Informasi Publik BRIDA NTB 2022",
      link: "/documents/ppid/DIP BRIDA NTB 2022.pdf",
    },
    {
      number: "20",
      namaInformasi: "SOP Umum dan Kepegawaian BRIDA NTB",
      link: "/documents/ppid/DAFTAR SOP UMUM DAN KEPEGAWIAN BRIDA PROV.pdf",
    },
    {
      number: "21",
      namaInformasi: "Tabel Kekuatan Pegawai BRIDA NTB 2022",
      link: "https://onedrive.live.com/embed?resid=7BD956663C2DF317%21177&authkey=%21AN94lEtlDOQhkLE&em=2&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True&wdInConfigurator=True&edesNext=false&ejss=false",
    },
    {
      number: "22",
      namaInformasi: "Naskah Akademik BRIDA",
      link: "/documents/Naskah Akademik BRIDA.pdf",
    },
    {
      number: "23",
      namaInformasi: "Pergub No.2 Tahun 2020 tentang pemberian beasiswa",
      link: "/documents/ppid/Pergub Nomor 2 Tahun 2020.pdf",
    },
    {
      number: "24",
      namaInformasi: "Indikator Kinerja Utama BRIDANTB",
      link: "https://drive.google.com/file/d/1XnuMoLxAo9sVIdp5x9APB_V3-0z_Sf5A/view?usp=sharing",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Informasi Tersedia Setiap Saat"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka Link"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
