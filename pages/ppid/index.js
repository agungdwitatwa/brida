import React from "react";
import Header from "../../components/Header/header";
import Navbar from "../../components/Navbar/NavbarPPID";
import Banner from "../../components/Banner/Banner";
import Footer from "../../components/Footer/Footer";
import s from "./ppid.module.css";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import Head from "next/head";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"PPID BRIDA Provinsi NTB"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <div className={s.title}>
          Pejabat Pengelola Informasi dan Dokumentasi
        </div>
        <div className={s.text}>
          PPID adalah kepanjangan dari Pejabat Pengelola Informasi dan
          Dokumentasi, dimana PPID berfungsi sebagai pengelola dan penyampai
          dokumen yang dimiliki oleh badan publik sesuai dengan amanat UU
          14/2008 tentang Keterbukaan Informasi Publik. Dengan keberadaan PPID
          maka masyarakat yang akan menyampaikan permohonan informasi lebih
          mudah dan tidak berbelit karena dilayani lewat satu pintu. Pejabat
          Pengelola Informasi dan Dokumentasi (PPID) adalah pejabat yang
          bertanggung jawab di bidang penyimpanan, pendokumentasian, penyediaan,
          dan/atau pelayanan informasi di badan publik.
        </div>
        <div className={s.text}>
          <b style={{ color: "#464646" }}>
            Pejabat Pengelola Informasi dan Dokumentasi Pembantu (PPID Pembantu)
          </b>
          adalah pejabat yang melaksanakan tugas dan fungsi sebagai PPID pada
          Satuan Organisasi Perangkat Daerah di lingkungan Pemerintah Daerah.
          PPID BRIDA adalah PPID Pembantu di lingkup Badan Riset dan Inovasi
          Daerah Provinsi Nusa Tenggara Barat.
        </div>
      </div>
      <div className={s.content}>
        <div className={s.title}>
          Menurut Pasal 13 Pergub NTB Nomor 24 Tahun 2018
        </div>
        <ol>
          <li>
            Setiap PPID wajib melakukan pendokumentasian, pengklasifikasian,
            pengelolaan informasi publik untuk mewujudkan pelayanan informasi
            secara cepat, tepat dan berkualitas.
          </li>
          <li>
            Untuk melaksanakan kewajiban sebagaimana dimaksud pada ayat (1) PPID
            Provinsi dapat menugaskan PPID Perangkat Daerah dan/atau pejabat
            fungsional.
          </li>
          <li>
            Untuk melaksanakan kewajiban sebagaimana yang dimaksud pada ayat (1)
            PPID Provinsi dan/atau PPID Perangkat Daerah wajib membangun dan
            mengembangkan sistem informasi dan dokumentasi informasi publik yang
            dapat diakses dengan mudah.
          </li>
          <li>
            Untuk mewujudkan pelayanan informasi secara cepat, tepat dan
            berkualitas sebagaimana dimaksud pada (1) PPID Provinsi dan/atau
            PPID Perangkat Daerah wajib memiliki website resmi.
          </li>
          <li>
            Website resmi sebagaimana dimaksud pada ayat (4) adalah merupakan
            satu kesatuan dengan website resmi Pemerintah untuk PPID Provinsi
            dan website resmi Perangkat Daerah untuk PPID perangkat Daerah.
          </li>
        </ol>
      </div>
      <div className={s.content}>
        <div className={s.title}>Maklumat Layanan PPID Brida Provinsi NTB</div>
        <iframe
          src="/documents/Maklumat Layanan PPID BRIDA PROVINSI NTB-dikonversi.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
