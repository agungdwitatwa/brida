import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./informasiPublik.module.css";
import { Stack } from "@mui/material";
import Link from "next/link";
import OpenInBrowserIcon from "@mui/icons-material/OpenInBrowser";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Peraturan Keterbukaan Informasi Publik"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <Stack spacing={4} marginBottom={15}>
        <div className={s.content}>
          <iframe
            src="/documents/UU Nomor 14 Tahun 2008.pdf#view=fitH"
            frameBorder="3"
            width={"100%"}
            height={"500px"}
            allowFullScreen
          ></iframe>
        </div>
        <div className={s.content}>
          <iframe
            src="/documents/BD Pergub Nomor 24 Tahun 2018.pdf#view=fitH"
            frameBorder="3"
            width={"100%"}
            height={"500px"}
            allowFullScreen
          ></iframe>
        </div>
        <div className={s.content}>
          <Stack spacing={2}>
            <Link
              href="/documents/Standar Biaya Layanan Informas - BRIDA NTB.pdf"
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <a style={{ color: "#10468f", textDecoration: "underline" }}>
                Standar Biaya Layanan Informasi
              </a>
            </Link>
            <Link
              href="/documents/ppid/SK PPID BRIDA NTB 2022.pdf"
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <a style={{ color: "#10468f", textDecoration: "underline" }}>
                SK Penetapan PPID BRIDA NTB
              </a>
            </Link>
            <Link
              href="/ppid/dokumen-ppid/standar-layanan"
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <a style={{ color: "#10468f", textDecoration: "underline" }}>
                SOP PPID
              </a>
            </Link>
          </Stack>
        </div>
      </Stack>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
