import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./style.module.css";
import Link from "next/link";
import OpenInBrowserIcon from "@mui/icons-material/OpenInBrowser";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Mekanisme Permohonan Informasi Publik"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={`${s.content} ${s.desc}`}>
        Kewajiban Badan Riset dan Inovasi Daerah Provinsi NTB adalah memberikan
        pelayanan informasi kepada pemohon informasi Publik secara cepat, tepat
        waktu, biaya proporsional dan cara yang mudah. Untuk dapat memperoleh
        salinan informasi yang Anda butuhkan, Anda dapat mengisi formulir yang
        ada dan mengikuti tata cara untuk memperoleh informasi publik dengan
        standar biaya layanan informasi yang berlaku.
      </div>
      <div className={s.content}>
        <iframe
          src="/layanan/pdf/TATA CARA PERMOHONAN INFORMASI PUBLIK.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
      </div>

      <div className={`${s.content} ${s.link}`}>
        <div>
          <Link
            // href="https://drive.google.com/file/d/1lwxHKiRAreo0lP6_RRx0hAEcvfnOT3pk/edit?usp=sharing"
            href="https://forms.gle/Hys3uDhYvUsXM2uD9"
            style={{ display: "flex", alignItems: "center" }}
          >
            <a>
              <OpenInBrowserIcon />
              Form Layanan Informasi Publik
            </a>
          </Link>
        </div>
        <div>
          <Link
            href="/documents/Standar Biaya Layanan Informas - BRIDA NTB.pdf"
            style={{ display: "flex", alignItems: "center" }}
          >
            <a>
              <OpenInBrowserIcon />
              Standar Biaya Layanan Informasi
            </a>
          </Link>
        </div>
      </div>

      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
