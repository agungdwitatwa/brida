import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./style.module.css";
import Link from "next/link";
import OpenInBrowserIcon from "@mui/icons-material/OpenInBrowser";

function Index() {
  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Pengajuan Keberatan Layanan Informasi Publik"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={`${s.content}`}>
        <div className={`${s.form}`}>
          <Link
            href="https://docs.google.com/forms/d/e/1FAIpQLScbGAF9DMwCPhwJsuk6yl-mSneQE_E3cRm0ebH3tGsYH0Fqig/viewform"
            style={{ display: "flex", alignItems: "center" }}
          >
            <a>
              <OpenInBrowserIcon />
              Form Pengajuan Keberatan Atas layanan informasi publik
            </a>
          </Link>
        </div>
        <iframe
          src="/documents/Alur Mekanisme Pengelolaan Keberatan.pdf#view=fitH"
          frameBorder="3"
          width={"100%"}
          height={"500px"}
          allowFullScreen
        ></iframe>
      </div>

      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
