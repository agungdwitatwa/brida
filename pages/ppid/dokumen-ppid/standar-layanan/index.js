import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./standarLayanan.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaLayanan", label: "Nama Layanan", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaLayanan: "Alur Mekanisme Permohonan Informasi Publik Secara Tertulis",
      link: "/documents/Alur Mekanisme Permohonan Informasi Publik Tertulis.pdf",
    },
    {
      number: "2",
      namaLayanan:
        "Alur Mekanisme Permohonan Informasi Publik Secara Tidak Tertulis",
      link: "/documents/Alur Mekanisme Permohonan Publik Tidak Tertulis.pdf",
    },
    {
      number: "3",
      namaLayanan: "Alur Mekanisme Pengelolaan Keberatan",
      link: "/documents/Alur Mekanisme Pengelolaan Keberatan.pdf",
    },
    {
      number: "4",
      namaLayanan: "Alur Mekanisme Pengecualian Informasi Publik",
      link: "/documents/Alur Mekanisme Tatacara Pengecualian Informasi Publik.pdf",
    },
    {
      number: "5",
      namaLayanan: "SOP Uji Konsekuensi Informasi yang dikecualikan",
      link: "/documents/SOP-UJI KONSEKUENSI INFORMASI-YANG-DIKECUALIKAN.pdf",
    },
    {
      number: "6",
      namaLayanan: "SOP Penetapan dan Pemutakhiran Daftar Informasi Publik",
      link: "/documents/SOP-PENETAPAN-DAN-PEMUTAKHIRAN-DIP.pdf",
    },
    {
      number: "7",
      namaLayanan: "SOP Penanganan Sengketa Informasi Publik",
      link: "/documents/SOP-PENANGANAN-SENGKETA-INFORMASI-PUBLIK.pdf",
    },
    {
      number: "8",
      namaLayanan: "SOP Pendokumentasian Informasi Publik",
      link: "/documents/SOP-PENDOKUMENTASIAN-INFORMASI-PUBLIK.pdf",
    },
    {
      number: "9",
      namaLayanan: "SOP Kunjungan Edu Wisata",
      link: "/documents/ppid/SOP EDUWISATA.pdf",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Standar Layanan"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka Dokumen"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
