import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./pengumuman.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "No.", minWidth: 10 },
    { id: "namaInformasi", label: "Nama Informasi", minWidth: 100 },
    { id: "tanggal", label: "Tanggal", minWidth: 100 },
    {
      id: "link",
      label: "Link",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "1",
      namaInformasi: "PK Eselon IV 2022_Perbaikan",
      tanggal: "25 Juli 2022",
      link: "https://drive.google.com/file/d/1zLKmfOoKgO49JjZZ_zWdvaRkkJjdE9RN/view?usp=sharing",
    },
    {
      number: "2",
      namaInformasi: "PK Eselon III Retno Untari",
      tanggal: "1 Agustus 2022",
      link: "https://drive.google.com/file/d/1hYZuAl-pBn9VcTY9sbyDEo60ct9wwpwW/view?usp=sharing",
    },
    {
      number: "3",
      namaInformasi: "PK Eselon III 2022",
      tanggal: "26 Juli 2022",
      link: "https://drive.google.com/file/d/1YsXBGWTGwN9J5YQC4qnWMkMAR1TGPxDs/view?usp=sharing",
    },
    {
      number: "4",
      namaInformasi: "PK Eselon II 2022_H.Wirawan",
      tanggal: "26 Juli 2022",
      link: "https://drive.google.com/file/d/1KchTfDjjiG5M6fFCTeHfzap9kWdnRVrI/view?usp=sharing",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Perjanjian Kinerja"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka File"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
