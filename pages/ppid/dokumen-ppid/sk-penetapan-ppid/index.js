import React from "react";

function Index() {
  return (
    <div>
      <iframe
        src="/documents/ppid/SK PPID BRIDA NTB 2022.pdf#view=fitH"
        frameBorder="3"
        width={"100%"}
        height={"600px"}
        allowFullScreen
      ></iframe>
    </div>
  );
}

export default Index;
