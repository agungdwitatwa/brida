import React from "react";
import Header from "../../../../components/Header/header";
import NavbarPPID from "../../../../components/Navbar/NavbarPPID";
import Banner from "../../../../components/Banner/Banner";
import Head from "next/head";
import Footer from "../../../../components/Footer/Footer";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import s from "./informasiLHKPN.module.css";
import Table from "../../../../components/Table/Table";

function Index() {
  const columns = [
    { id: "number", label: "Nama", minWidth: 10 },
    { id: "namaInformasi", label: "Jabatan", minWidth: 100 },
    {
      id: "link",
      label: "LHKPN",
      minWidth: 170,
      align: "left",
    },
  ];

  const rows = [
    {
      number: "Wirawan,S.Si.MT",
      namaInformasi: "Kepala Badan",
      link: "/documents/kaban/LHKPN_435286_WIRAWAN.pdf",
    },
    {
      number: "Retno Untari, S.Si, M.Kes",
      namaInformasi: "Sekretaris Badan",
      link: "/documents/sekban/LHKPN_174388_RETNO UNTARI.pdf",
    },
    {
      number: "Lalu Suryadi S.SP.MM.",
      namaInformasi:
        "Kepala Bidang Penelitian Pengembangan Inovasi dan Teknologi",
      link: "/documents/kabid/LHKPN_521222_L.SURYADI.pdf",
    },
    {
      number: "Ahmad Muslim, S.Pd",
      namaInformasi:
        "Kepala Bidang Pengembangan Sumber Daya Ilmu Pengetahuan dan Teknologi",
      link: "/documents/kabid/Pengumuman_Harta_Kekayaan_LHKPN_789543.pdf",
    },

    {
      number: "Lale Ira Amrita Sari, ST",
      namaInformasi: "Kepala Bidang Pemanfaatan Riset dan Inovasi",
      link: "/documents/kabid/LHKPN_788833_LALE IRA.pdf",
    },
    {
      number: "Iskandar Sukmana",
      namaInformasi: "Kepala Bidang Kemitraan dan Inkubasi Bisnis",
      link: "/documents/kabid/LHKPN_ISKANDAR SUKMANA.pdf",
    },
  ];

  return (
    <div className={s.container}>
      <Head>
        <title>
          PPID | BRIDA NTB - Badan Riset dan Inovasi Daerah Nusa Tenggara Barat
        </title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <NavbarPPID />
      <Banner
        name={"Informasi LHKPN"}
        icon={<AdminPanelSettingsIcon fontSize="large" />}
        style={{ color: "#10468f", backgroundImage: "none" }}
      />
      <div className={s.content}>
        <Table columns={columns} rows={rows} titleLink={"Buka File"} />
      </div>
      <Footer footerColor={"#10468f"} />
    </div>
  );
}

export default Index;
