import React from "react";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import s from "./detail.module.css";
import Footer from "../../../components/Footer/Footer";
import { createClient } from "contentful";
import { BLOCKS, INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import ArticleIcon from "@mui/icons-material/Article";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import TodayIcon from "@mui/icons-material/Today";
import Head from "next/head";
import Image from "next/image";

const client = createClient({
  space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
  accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
});

const renderOptions = {
  renderNode: {
    [BLOCKS.EMBEDDED_ASSET]: (node, children) => {
      return (
        <div className={s.header}>
          <img
            src={`https://${node.data.target.fields.file.url}`}
            width="50%"
            alt={node.data.target.fields.description}
          />
        </div>
      );
    },
  },
};

export const getStaticPaths = async () => {
  const res = await client.getEntries({
    content_type: "eduWisata",
  });

  const paths = res.items.map((item) => {
    return {
      params: { slug: item.fields.slug },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export async function getStaticProps({ params }) {
  const { items } = await client.getEntries({
    content_type: "eduWisata",
    "fields.slug": params.slug,
    select: "fields",
  });

  return {
    props: {
      berita: items[0],
    },
  };
}

const wImage = 400;

function slug({ berita }) {
  return (
    <div className={s.container}>
      <Head>
        <title>{berita.fields.judulBerita}</title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="BRIDA NTB - Badan Riset dan Inovasi Daerah NTB"
        />
        <meta name="author" content="PPID - BRIDA NTB" />
        <meta name="developer" content="Agung Dwitatwa" />
        <meta name="Content Writer" content="Nabilah Wahyunan" />
      </Head>
      <Header />
      <Navbar />
      <Banner
        name={"Berita Seputar BRIDA Provinsi NTB"}
        icon={<ArticleIcon fontSize="large" />}
      />
      <div className={s.content}>
        <div className={s.content_wrapper}>
          <div className={s.judul}>
            <div>{berita.fields.judulBerita}</div>
            <div className={s.status}>
              <div>
                <AccountCircleIcon /> gandysmarisha
              </div>
              <div>
                <TodayIcon />{" "}
                {berita.fields.tanggalRelease.split("-").reverse().join("-")}
              </div>
            </div>
          </div>
          <div className={s.header}>
            <Image
              src={`https:${berita.fields.coverBerita.fields.file.url}`}
              width={wImage}
              height={wImage}
            />
          </div>
          <div className={s.isiBerita}>
            {documentToReactComponents(berita.fields.isiBerita, renderOptions)}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default slug;
