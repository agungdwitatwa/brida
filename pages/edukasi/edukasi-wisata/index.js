import React from "react";
import s from "./eduWisata.module.css";
import Header from "../../../components/Header/header";
import Navbar from "../../../components/Navbar/navbar";
import Banner from "../../../components/Banner/Banner";
import CastForEducationIcon from "@mui/icons-material/CastForEducation";
import { createClient } from "contentful";
import InfiniteScroll from "react-infinite-scroll-component";
import { useEffect, useState } from "react";
import Card from "../../../components/Card/EduWisata";
import Footer from "../../../components/Footer/Footer";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
    accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
  });
  const res = await client.getEntries({
    content_type: "eduWisata",
    limit: 4,
  });

  return {
    props: {
      data: res.items,
      data_total: res.total,
      spaceID: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE,
      token: process.env.NEXT_PUBLIC_CONTENTFUL_TOKEN,
    },
  };
}

function BeritaComp({ data, data_total, spaceID, token }) {
  const [berita, setBerita] = useState(data);
  const [hasMore, setHasMore] = useState(true);

  const getMoreBerita = async () => {
    const clnt = createClient({
      space: spaceID,
      accessToken: token,
    });
    const res = await clnt.getEntries({
      content_type: "eduWisata",
      skip: berita.length,
      limit: 4,
    });
    setBerita((berita) => [...berita, ...res.items]);
  };

  useEffect(() => {
    setHasMore(data_total > berita.length ? true : false);
  }, [berita]);
  return (
    <div className={s.container}>
      <Header />
      <Navbar />
      <Banner
        name={"Edukasi Wisata"}
        icon={<CastForEducationIcon fontSize="large" />}
      />
      <InfiniteScroll
        dataLength={berita.length}
        next={getMoreBerita}
        hasMore={hasMore}
        loader={<h1>Loading . . . </h1>}
        className={s.berita_container}
      >
        {berita.map((item) => (
          <div key={item.sys.id} className={s.berita_container_item}>
            <Card
              judulBerita={item.fields.judulBerita}
              cover={item.fields.coverBerita.fields.file.url}
              link={item.fields.slug}
              className={s.berita_item}
            />
          </div>
        ))}
      </InfiniteScroll>
      <Footer />
    </div>
  );
}

export default BeritaComp;
