import "../styles/globals.css";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <NextNProgress color="#e43836" height={7} />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
