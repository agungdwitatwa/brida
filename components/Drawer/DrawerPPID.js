import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItem from "@mui/material/ListItem";
import HomeIcon from "@mui/icons-material/Home";
import MenuIcon from "@mui/icons-material/Menu";
import s from "./drawer.module.css";
import Menu from "../Menu/menu";
import ArticleIcon from "@mui/icons-material/Article";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import AssignmentTurnedInIcon from "@mui/icons-material/AssignmentTurnedIn";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";

export default function DrawerMenu() {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      // onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <Menu name={"Beranda"} link="/ppid" />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <AssignmentIndIcon />
          </ListItemIcon>
          <Menu
            name={"Profil PPID"}
            sub={[
              [
                "Visi Misi PPID",
                "/ppid/profil-ppid/visi-misi",
                Math.floor(Math.random * 100),
              ],
              [
                "Tugas dan Fungsi PPID",
                "/ppid/profil-ppid/tugas-dan-fungsi",
                Math.floor(Math.random * 100),
              ],
              [
                "Struktur PPID",
                "/ppid/profil-ppid/struktur-ppid",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <ArticleIcon />
          </ListItemIcon>
          <Menu
            name={"Jenis Informasi"}
            sub={[
              [
                "Daftar Informasi Publik",
                "/ppid/jenis-informasi/daftar-informasi-publik",
                Math.floor(Math.random * 100),
              ],
              [
                "Informasi Berkala",
                "/ppid/jenis-informasi/informasi-berkala",
                Math.floor(Math.random * 100),
              ],
              [
                "Informasi Serta Merta",
                "/ppid/jenis-informasi/informasi-serta-merta",
                Math.floor(Math.random * 100),
              ],
              [
                "Informasi Tersedia Setiap Saat",
                "/ppid/jenis-informasi/informasi-setiap-saat",
                Math.floor(Math.random * 100),
              ],
              [
                "Agenda Harian Kepala Badan",
                "/ppid/jenis-informasi/agenda-harian-kepala-badan",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <AssignmentTurnedInIcon />
          </ListItemIcon>
          <Menu
            name={"Dokumen PPID"}
            sub={[
              [
                "Pengumuman Penting",
                "/ppid/dokumen-ppid/pengumuman-penting",
                Math.floor(Math.random * 100),
              ],
              [
                "SK/Penetapan PPID",
                "https://drive.google.com/file/d/1j64XTZK5whpwpus6KQTRfkjI1jNpM7yu/view?usp=sharing",
                Math.floor(Math.random * 100),
              ],
              [
                "Informasi Keuangan",
                "https://drive.google.com/drive/folders/1LOmcRhIxpcBxViWfMU7i4SKGPJLKlnAD?usp=sharing",
                Math.floor(Math.random * 100),
              ],
              [
                "Laporan Kinerja",
                "/documents/ppid/RKPD Triwulan II 2022 - BRIDA.pdf",
                Math.floor(Math.random * 100),
              ],
              [
                "Renja & Renstra",
                "/ppid/dokumen-ppid/renja-renstra",
                Math.floor(Math.random * 100),
              ],
              [
                "Standar Layanan",
                "/ppid/dokumen-ppid/standar-layanan",
                Math.floor(Math.random * 100),
              ],
              ["Perjanjian Kerjasama", "#", Math.floor(Math.random * 100)],
              ["Laporan Layanan Informasi", "#", Math.floor(Math.random * 100)],
              [
                "Informasi LHKPN",
                "/ppid/dokumen-ppid/informasi-lhkpn",
                Math.floor(Math.random * 100),
              ],
              [
                "Anggaran PPID",
                "https://drive.google.com/drive/folders/1uWW0YvWbcviHAVLaNcOlKbFKFynExXwe?usp=sharing",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <MiscellaneousServicesIcon />
          </ListItemIcon>
          <Menu
            name={"Prosedur PPID"}
            sub={[
              [
                "Mekanisme Permohonan Informasi Publik",
                "/ppid/prosedur-ppid/mekanisme-permohonan-informasi-publik",
                Math.floor(Math.random * 100),
              ],
              [
                "Pengajuan Keberatan Layanan Informasi Publik",
                "/ppid/prosedur-ppid/pengajuan-keberatan",
                Math.floor(Math.random * 100),
              ],
              [
                "Penyelesaian Sengketa",
                "/ppid/prosedur-ppid/penyelesaian-sengketa",
                Math.floor(Math.random * 100),
              ],
              [
                "Peraturan Keterbukaan Informasi Publik",
                "/ppid/prosedur-ppid/peraturan-keterbukaan-informasi-publik",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <ArticleIcon />
          </ListItemIcon>
          <Menu
            name={"SI-PPID"}
            link="https://v2.ppid.ntbprov.go.id/halaman-25-regulasi.html"
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <ArticleIcon />
          </ListItemIcon>
          <Menu
            name={"SAQ PPID 2022"}
            link="https://docs.google.com/spreadsheets/d/e/2PACX-1vQuZCV-urg_f0QGWYiJkChhhLgec33otWaRLrm5qYqrQb_WrYy_PT8Tp_axeNt4Dq_JI8XJOu4sqXzV/pubhtml?gid=2005787319&single=true"
          />
        </ListItem>
      </List>
    </Box>
  );

  return (
    <div className={s.container}>
      <React.Fragment key={"left"}>
        <Button startIcon={<MenuIcon />} onClick={toggleDrawer("left", true)}>
          {"Menu"}
        </Button>
        <Drawer
          anchor={"left"}
          open={state["left"]}
          onClose={toggleDrawer("left", false)}
        >
          {list("left")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
