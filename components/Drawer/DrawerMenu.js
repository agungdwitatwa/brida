import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItem from "@mui/material/ListItem";
import HomeIcon from "@mui/icons-material/Home";
import MenuIcon from "@mui/icons-material/Menu";
import EventNoteIcon from "@mui/icons-material/EventNote";
import s from "./drawer.module.css";
import Menu from "../Menu/menu";
import ArticleIcon from "@mui/icons-material/Article";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import AssignmentTurnedInIcon from "@mui/icons-material/AssignmentTurnedIn";
import GroupsIcon from "@mui/icons-material/Groups";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import CastForEducationIcon from "@mui/icons-material/CastForEducation";

export default function DrawerMenu() {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <EventNoteIcon />
          </ListItemIcon>
          <Menu name={"Pekan PIRN"} link="https://pirn.brida.ntbprov.go.id" />
        </ListItem>

        <ListItem className={s.listItem}>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <Menu name={"Beranda"} link="/" />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <AssignmentIndIcon />
          </ListItemIcon>
          <Menu
            name={"Profil"}
            sub={[
              [
                "Profil Badan",
                "/profil/profil-badan",
                Math.floor(Math.random * 100),
              ],
              [
                "Profil Kepala Badan",
                "/profil/profil-kepala-badan",
                Math.floor(Math.random * 100),
              ],
              [
                "Profil Sekretaris Badan",
                "/profil/profil-sekretaris-badan",
                Math.floor(Math.random * 100),
              ],
              [
                "Profil Kasubbag Umum",
                "/profil/profil-kepala-sub-bagian-umum",
                Math.floor(Math.random * 100),
              ],
              [
                "Profil Jabatan Fungsional",
                "/profil/profil-jabatan-fungsional",
                Math.floor(Math.random * 100),
              ],
              [
                "Profil Bidang",
                "/profil/profil-bidang",
                Math.floor(Math.random * 100),
              ],
              [
                "Struktur Organisasi",
                "/profil/struktur-organisasi",
                Math.floor(Math.random * 100),
              ],
              [
                "Kalender Kegiatan BRIDA",
                "https://docs.google.com/spreadsheets/d/e/2PACX-1vQ38fDyLwv-DjU-jMrXfczY9PJ13TMKzRiLpfWdoW4oBq1tUNjk0zFkQZT3OyQKn6QWEKj7rq4wexgN/pubhtml",
                Math.floor(Math.random * 100),
              ],
              [
                "Maklumat Pelayanan",
                "/profil/maklumat-pelayanan",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <ArticleIcon />
          </ListItemIcon>
          <Menu name={"Berita"} link="/berita" />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <AssignmentTurnedInIcon />
          </ListItemIcon>
          <Menu name={"Program"} link="/program" />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <GroupsIcon />
          </ListItemIcon>
          <Menu
            name={"Mitra"}
            sub={[
              [
                "Daftar Mitra",
                "/mitra/daftar-mitra",
                Math.floor(Math.random * 100),
              ],
              [
                "Produk Mitra",
                "/mitra/daftar-produk",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <AdminPanelSettingsIcon />
          </ListItemIcon>
          <Menu name={"PPID"} link="/ppid" />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <MiscellaneousServicesIcon />
          </ListItemIcon>
          <Menu
            name={"Layanan"}
            sub={[
              [
                "Layanan Penelitian",
                "/layanan/layanan-penelitian",
                Math.floor(Math.random * 100),
              ],
              [
                "Layanan Kemitraan dan Inkubasi",
                "/layanan/layanan-kemitraan-dan-inkubasi",
                Math.floor(Math.random * 100),
              ],
              [
                "Layanan Kepuasan Masyarakat",
                "/layanan/layanan-kepuasan-masyarakat",
                Math.floor(Math.random * 100),
              ],
              [
                "Layanan Informasi Publik",
                "/layanan/layanan-informasi-publik",
                Math.floor(Math.random * 100),
              ],
              ["Layanan Pengaduan", "/layanan/layanan-pengaduan"],
              ["Layanan Magang", "https://forms.gle/x9tso2rkAyTHTVrZ6"],
            ]}
          />
        </ListItem>
        <ListItem className={s.listItem}>
          <ListItemIcon>
            <CastForEducationIcon />
          </ListItemIcon>
          <Menu
            name={"Edukasi"}
            sub={[
              [
                "Edukasi Wisata",
                "/edukasi/edukasi-wisata",
                Math.floor(Math.random * 100),
              ],
            ]}
          />
        </ListItem>
      </List>
    </Box>
  );

  return (
    <div className={s.container}>
      <React.Fragment key={"left"}>
        <Button startIcon={<MenuIcon />} onClick={toggleDrawer("left", true)}>
          {"Menu"}
        </Button>
        <Drawer
          anchor={"left"}
          open={state["left"]}
          onClose={toggleDrawer("left", false)}
        >
          {list("left")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
