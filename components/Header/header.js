import React from "react";
import s from "./header.module.css";
import Image from "next/image";
import FacebookIcon from "@mui/icons-material/Facebook";
import YouTubeIcon from "@mui/icons-material/YouTube";
import InstagramIcon from "@mui/icons-material/Instagram";
import Link from "next/link";

function header({ ...props }) {
  return (
    <div className={s.container} {...props}>
      <div className={s.logo}>
        <div className={s.image}>
          <Image src="/logo pemprov.png" width={"30"} height={"40"} />
        </div>
        <div className={s.image}>
          <Image src="/logo BRIN.png" width={"40"} height={"40"} />
        </div>
        <div className={s.label}>
          <span>BRIDA</span> Provinsi NTB
        </div>
      </div>
      <div className={s.sosmed}>
        <Link href="https://facebook.com/BRIDA-Nusa-Tenggara-Barat-103262571862743">
          <a>
            <div className={s.item}>
              <FacebookIcon />
            </div>
          </a>
        </Link>
        <Link href="https://www.instagram.com/brida_ntb/">
          <a>
            <div className={s.item}>
              <InstagramIcon />
            </div>
          </a>
        </Link>
        <Link href="https://www.youtube.com/channel/UC080LWwxcuQ1pmSORgDEkWQ">
          <a>
            <div className={s.item}>
              <YouTubeIcon />
            </div>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default header;
