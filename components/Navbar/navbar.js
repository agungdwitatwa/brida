import React from "react";
import s from "./navbar.module.css";
import Menu from "../Menu/menu";
import MailIcon from "@mui/icons-material/Mail";
import Button from "@mui/material/Button";
import DrawerMenu from "../Drawer/DrawerMenu";

function navbar() {
  return (
    <div className={s.container}>
      <div className={s.drawer}>
        <DrawerMenu />
      </div>
      <div className={s.menu}>
        <Menu name={"Pekan PIRN"} link="https://pirn.brida.ntbprov.go.id" />
        <Menu name={"Beranda"} link="/" />
        <Menu
          name={"Profil"}
          sub={[
            [
              "Profil Badan",
              "/profil/profil-badan",
              Math.floor(Math.random * 100),
            ],
            [
              "Profil Kepala Badan",
              "/profil/profil-kepala-badan",
              Math.floor(Math.random * 100),
            ],
            [
              "Profil Sekretaris Badan",
              "/profil/profil-sekretaris-badan",
              Math.floor(Math.random * 100),
            ],
            [
              "Profil Kasubbag Umum",
              "/profil/profil-kepala-sub-bagian-umum",
              Math.floor(Math.random * 100),
            ],
            [
              "Profil Jabatan Fungsional",
              "/profil/profil-jabatan-fungsional",
              Math.floor(Math.random * 100),
            ],
            [
              "Profil Bidang",
              "/profil/profil-bidang",
              Math.floor(Math.random * 100),
            ],
            [
              "Struktur Organisasi",
              "/profil/struktur-organisasi",
              Math.floor(Math.random * 100),
            ],
            [
              "Kalender Kegiatan BRIDA",
              "https://docs.google.com/spreadsheets/d/e/2PACX-1vQ38fDyLwv-DjU-jMrXfczY9PJ13TMKzRiLpfWdoW4oBq1tUNjk0zFkQZT3OyQKn6QWEKj7rq4wexgN/pubhtml",
              Math.floor(Math.random * 100),
            ],
            [
              "Maklumat Pelayanan",
              "/profil/maklumat-pelayanan",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu name={"Berita"} link="/berita" />
        <Menu name={"Program"} link="/program" />
        <Menu
          name={"Mitra"}
          sub={[
            [
              "Daftar Mitra",
              "/mitra/daftar-mitra",
              Math.floor(Math.random * 100),
            ],
            [
              "Produk Mitra",
              "/mitra/daftar-produk",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu name={"PPID"} link="/ppid" />
        <Menu
          name={"Layanan"}
          sub={[
            [
              "Layanan Penelitian",
              "/layanan/layanan-penelitian",
              Math.floor(Math.random * 100),
            ],
            [
              "Layanan Kemitraan dan Inkubasi",
              "/layanan/layanan-kemitraan-dan-inkubasi",
              Math.floor(Math.random * 100),
            ],
            [
              "Layanan Kepuasan Masyarakat",
              "/layanan/layanan-kepuasan-masyarakat",
              Math.floor(Math.random * 100),
            ],
            [
              "Layanan Informasi Publik",
              "/layanan/layanan-informasi-publik",
              Math.floor(Math.random * 100),
            ],
            ["Layanan Pengaduan", "/layanan/layanan-pengaduan"],
            ["Layanan Magang", "https://forms.gle/x9tso2rkAyTHTVrZ6"],
          ]}
        />
        <Menu
          name={"Edukasi"}
          sub={[
            [
              "Edukasi Wisata",
              "/edukasi/edukasi-wisata",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
      </div>
      <div className={s.mail}>
        <Button
          variant="text"
          startIcon={<MailIcon />}
          href="mailto:brida.ntbprov@gmail.com"
        >
          Email
        </Button>
      </div>
    </div>
  );
}

export default navbar;
