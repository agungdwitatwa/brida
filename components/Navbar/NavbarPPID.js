import React from "react";
import s from "./navbarPPID.module.css";
import Menu from "../Menu/menu";
import HomeIcon from "@mui/icons-material/Home";
import Button from "@mui/material/Button";
import DrawerMenu from "../Drawer/DrawerPPID";
import Link from "next/link";

function NavbarPPID() {
  return (
    <div className={s.container}>
      <div className={s.drawer}>
        <DrawerMenu />
      </div>
      <div className={s.menu}>
        <Menu name={"Beranda"} link="/ppid" />
        <Menu
          name={"Profil PPID"}
          sub={[
            [
              "Visi Misi PPID",
              "/ppid/profil-ppid/visi-misi",
              Math.floor(Math.random * 100),
            ],
            [
              "Tugas dan Fungsi PPID",
              "/ppid/profil-ppid/tugas-dan-fungsi",
              Math.floor(Math.random * 100),
            ],
            [
              "Struktur PPID",
              "/ppid/profil-ppid/struktur-ppid",
              Math.floor(Math.random * 100),
            ],
            [
              "Daftar Layanan Informasi Publik",
              "/ppid/profil-ppid/daftar-layanan-informasi-publik",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu
          name={"Jenis Informasi"}
          sub={[
            [
              "Daftar Informasi Publik",
              "/ppid/jenis-informasi/daftar-informasi-publik",
              Math.floor(Math.random * 100),
            ],
            [
              "Informasi Berkala",
              "/ppid/jenis-informasi/informasi-berkala",
              Math.floor(Math.random * 100),
            ],
            [
              "Informasi Serta Merta",
              "/ppid/jenis-informasi/informasi-serta-merta",
              Math.floor(Math.random * 100),
            ],
            [
              "Informasi Tersedia Setiap Saat",
              "/ppid/jenis-informasi/informasi-setiap-saat",
              Math.floor(Math.random * 100),
            ],
            [
              "Agenda Harian Kepala Badan",
              "/ppid/jenis-informasi/agenda-harian-kepala-badan",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu
          name={"Dokumen PPID"}
          sub={[
            [
              "Pengumuman Penting",
              "/ppid/dokumen-ppid/pengumuman-penting",
              Math.floor(Math.random * 100),
            ],
            [
              "SK/Penetapan PPID",
              "https://drive.google.com/file/d/1j64XTZK5whpwpus6KQTRfkjI1jNpM7yu/view?usp=sharing",
              Math.floor(Math.random * 100),
            ],
            [
              "Informasi Keuangan",
              "https://drive.google.com/drive/folders/1LOmcRhIxpcBxViWfMU7i4SKGPJLKlnAD?usp=sharing",
              Math.floor(Math.random * 100),
            ],
            [
              "Laporan Kinerja",
              "/documents/ppid/RKPD Triwulan II 2022 - BRIDA.pdf",
              Math.floor(Math.random * 100),
            ],
            [
              "Renja & Renstra",
              "/ppid/dokumen-ppid/renja-renstra",
              Math.floor(Math.random * 100),
            ],
            [
              "Standar Layanan",
              "/ppid/dokumen-ppid/standar-layanan",
              Math.floor(Math.random * 100),
            ],
            [
              "Perjanjian Kinerja",
              "/ppid/dokumen-ppid/perjanjian-kinerja",
              Math.floor(Math.random * 100),
            ],
            [
              "Laporan Layanan Informasi",
              "/ppid/dokumen-ppid/laporan-layanan-informasi",
              Math.floor(Math.random * 100),
            ],
            [
              "Informasi LHKPN",
              "/ppid/dokumen-ppid/informasi-lhkpn",
              Math.floor(Math.random * 100),
            ],
            [
              "Anggaran PPID",
              "https://drive.google.com/drive/folders/1uWW0YvWbcviHAVLaNcOlKbFKFynExXwe?usp=sharing",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu
          name={"Prosedur PPID"}
          sub={[
            [
              "Mekanisme Permohonan Informasi Publik",
              "/ppid/prosedur-ppid/mekanisme-permohonan-informasi-publik",
              Math.floor(Math.random * 100),
            ],
            [
              "Pengajuan Keberatan Layanan Informasi Publik",
              "/ppid/prosedur-ppid/pengajuan-keberatan",
              Math.floor(Math.random * 100),
            ],
            [
              "Penyelesaian Sengketa",
              "/ppid/prosedur-ppid/penyelesaian-sengketa",
              Math.floor(Math.random * 100),
            ],
            [
              "Peraturan Keterbukaan Informasi Publik",
              "/ppid/prosedur-ppid/peraturan-keterbukaan-informasi-publik",
              Math.floor(Math.random * 100),
            ],
          ]}
        />
        <Menu
          name={"SI PPID"}
          link="https://v2.ppid.ntbprov.go.id/halaman-25-regulasi.html"
        />
        <Menu
          name={"SAQ PPID 2022"}
          link="https://docs.google.com/spreadsheets/d/e/2PACX-1vQuZCV-urg_f0QGWYiJkChhhLgec33otWaRLrm5qYqrQb_WrYy_PT8Tp_axeNt4Dq_JI8XJOu4sqXzV/pubhtml?gid=2005787319&single=true"
        />
      </div>
      <div className={s.mail}>
        <Link href="/">
          <a>
            <Button variant="text" startIcon={<HomeIcon />}>
              Halaman Utama
            </Button>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default NavbarPPID;
