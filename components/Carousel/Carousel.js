import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Navigation, Pagination, Mousewheel, Keyboard } from "swiper";
import s from "./carousel.module.css";
import Link from "next/link";

function Carousel() {
  return (
    <div className={s.container}>
      <Swiper
        cssMode={true}
        navigation={true}
        pagination={true}
        mousewheel={true}
        keyboard={true}
        modules={[Navigation, Pagination, Mousewheel, Keyboard]}
        className="mySwiper"
      >
        <SwiperSlide>
          <img src="/Visitor.jpg" width={"100%"} />
        </SwiperSlide>
        {/* <SwiperSlide>
          <img src="/banner.png" width={"100%"} />
        </SwiperSlide> */}
        <SwiperSlide>
          <Link href="https://drive.google.com/file/d/1wgtnuAc3o7QQUxb0QdThzz0Ro-7s5mAO/view?usp=sharing">
            <a>
              <img src="/image/bulletin.png" width={"100%"} />
            </a>
          </Link>
        </SwiperSlide>
        <SwiperSlide>
          <Link href="https://docs.google.com/document/d/131uJQRppgHB30vS4G_V0tofZoWm5LS61xpaFQM0S2yI/edit?usp=sharing">
            <a>
              <img
                src="https://drive.google.com/uc?export=view&id=1-D2aFsMJ4_TA_zj6gu-lP477gkxqCdrn"
                width={"100%"}
              />
            </a>
          </Link>
        </SwiperSlide>
        <SwiperSlide>
          <img src="/image/Banner.jpg" width={"100%"} />
        </SwiperSlide>
      </Swiper>
    </div>
  );
}

export default Carousel;
