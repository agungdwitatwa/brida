import { React } from "react";
import { useState } from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Link from "next/link";
import s from "./menu.module.css";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";

export default function MenuComp({ name, sub, link = "#menu" }) {
  const [anchorEl, setAnchorEl] = useState("");
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Link href={link}>
        <a>
          <Button
            id="basic-button"
            aria-controls={open ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
            className={s.button}
            sx={{
              color: "#fff",
              fontWeight: "600",
              fontFamily: "Roboto, sans-serif",
              textTransform: "capitalize",
            }}
            endIcon={sub ? <ArrowDropDownIcon /> : ""}
          >
            {name}
          </Button>
        </a>
      </Link>

      {sub ? (
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
          className={s.menu}
        >
          {sub.map((item, index) => (
            <Link href={`${item[1]}`} key={`${index}`}>
              <a>
                <MenuItem onClick={handleClose}>{item[0]}</MenuItem>
              </a>
            </Link>
          ))}
        </Menu>
      ) : (
        <></>
      )}
    </div>
  );
}
