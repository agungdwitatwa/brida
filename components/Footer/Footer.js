import React from "react";
import s from "../Footer/footer.module.css";
import HomeIcon from "@mui/icons-material/Home";
import EmailIcon from "@mui/icons-material/Email";
import PhoneIcon from "@mui/icons-material/Phone";
import FacebookIcon from "@mui/icons-material/Facebook";
import YouTubeIcon from "@mui/icons-material/YouTube";
import InstagramIcon from "@mui/icons-material/Instagram";
import WatchLaterIcon from "@mui/icons-material/WatchLater";
import Link from "next/link";
// import countapi from "countapi-js";

function Footer({ footerColor }) {
  // countapi.hit("brida.ntbprov.go.id", "").then((result) => {
  //   console.log(result);
  // });
  return (
    <div className={s.container}>
      <div className={s.up}>
        <div className={s.left}>
          <div className={s.logo}>
            <img src="/logo pemprov.png" />
            <img src="/logo BRIN.png" />
            <div>
              <span>BRIDA</span> Provinsi NTB
            </div>
          </div>
          <div className={s.desc}>
            <div className={s.item_first}>
              <HomeIcon /> Jalan Raya Zamia, No.2 Desa Lelede, Lombok Barat,
              Nusa Tenggara Barat
            </div>
            <div className={s.item}>
              <EmailIcon /> brida.ntbprov@gmail.com
            </div>
            <div className={s.item}>
              <PhoneIcon /> +62 878 5553 9094
            </div>
            <div className={s.item}>
              <WatchLaterIcon />
              <div>
                <div>Senin - Kamis, 08:00 - 16:00 WITA</div>
                <div>Jumat, 08:00 - 17:00 WITA</div>
              </div>
            </div>
          </div>
        </div>
        <div className={s.right}>
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.4946360398076!2d116.1130116150531!3d-8.644415993788227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdbf89c5193d35%3A0x114a8b208a78c166!2sBadan%20Riset%20dan%20Inovasi%20Daerah%20(BRIDA)%20NTB!5e0!3m2!1sen!2sid!4v1646223564668!5m2!1sen!2sid"
            width="600"
            height="200"
            //   style="border:0;"
            style={{ border: 0 }}
            allowFullScreen=""
            loading="lazy"
            className={s.maps}
          ></iframe>
        </div>
      </div>
      <div className={s.down} style={{ backgroundColor: footerColor }}>
        <div>
          &copy; {new Date().getFullYear()}, Badan Riset dan Inovasi Daerah
          Provinsi NTB
        </div>
        <div className={s.sosmed}>
          <Link href="https://facebook.com/BRIDA-Nusa-Tenggara-Barat-103262571862743">
            <a>
              <div className={s.item}>
                <FacebookIcon />
              </div>
            </a>
          </Link>
          <Link href="https://www.instagram.com/brida_ntb/">
            <a>
              <div className={s.item}>
                <InstagramIcon />
              </div>
            </a>
          </Link>
          <Link href="https://www.youtube.com/channel/UC080LWwxcuQ1pmSORgDEkWQ">
            <a>
              <div className={s.item}>
                <YouTubeIcon />
              </div>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Footer;
