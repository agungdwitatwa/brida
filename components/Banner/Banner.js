import React from "react";
import s from "./banner.module.css";

function Banner({ name, icon, ...props }) {
  return (
    <div className={s.banner} {...props}>
      <h3>
        {icon}
        {name}
      </h3>
    </div>
  );
}

export default Banner;
