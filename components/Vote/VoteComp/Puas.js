import React, { useContext, useEffect } from "react";
import { Box, Typography, Stack } from "@mui/material";
import { indigo } from "@mui/material/colors";
import { VoteWrapperContext } from "../VoteWrapper";
import { createClient } from "contentful-management";

const client = createClient({
  accessToken: "CFPAT-AfZMtyg8WUmt7pDPXhfqk4TGJgJ3KYX8UvJRiE6fx30",
});

function Puas() {
  const { jumlahVote, setJumlahVote, isVoted, setIsVoted, ipAddress } =
    useContext(VoteWrapperContext);

  const addVote = async () => {
    await client
      .getSpace("he0vccpsw0h1")
      .then((space) => space.getEnvironment("master"))
      .then((environment) =>
        environment.createEntry("puas", {
          fields: {
            ipAdress: {
              "en-US": ipAddress,
            },
          },
        })
      )
      .then((entry) => console.log(entry))
      .catch(console.error);
  };

  const handleClick = () => {
    console.log(jumlahVote.puas);
    setIsVoted(true);
    setJumlahVote({ ...jumlahVote, puas: jumlahVote.puas + 1 });
    addVote();
  };

  return (
    <>
      {isVoted ? (
        <Box
          sx={{
            borderRadius: 2,
            height: 100,
            padding: 2,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: indigo[600],
            width: "100%",
          }}
        >
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography color="white"> Puas </Typography>
            <Box
              sx={{
                backgroundColor: "white",
                color: indigo[600],
                borderRadius: "50%",
                height: 60,
                width: 60,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                padding: 1,
              }}
            >
              <Typography color={indigo[600]} fontSize={13} fontWeight={600}>
                {jumlahVote.puas}
              </Typography>
            </Box>
          </Stack>
        </Box>
      ) : (
        <Box
          onClick={handleClick}
          sx={{
            borderRadius: 2,
            height: 100,
            padding: 2,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            border: `3px solid ${indigo[600]}`,
            width: "100%",
            cursor: "pointer",
          }}
        >
          <Typography color={indigo[600]}> Puas</Typography>
        </Box>
      )}
    </>
  );
}

export default Puas;
