import { createContext, useState, useEffect } from "react";
import Box from "@mui/material/Box";
import SangatPuas from "./VoteComp/SangatPuas";
import CukupPuas from "./VoteComp/CukupPuas";
import Puas from "./VoteComp/Puas";
import KurangPuas from "./VoteComp/KurangPuas";
import TidakPuas from "./VoteComp/TidakPuas";
import { createClient } from "contentful-management";
const client = createClient({
  accessToken: "CFPAT-AfZMtyg8WUmt7pDPXhfqk4TGJgJ3KYX8UvJRiE6fx30",
});
import Skeleton from "@mui/material/Skeleton";

export const VoteWrapperContext = createContext();

function VoteWrapper({ ipAddress }) {
  const [isLoading, setIsLoading] = useState(true);
  const [jumlahVote, setJumlahVote] = useState({
    sangatPuas: 0,
    cukupPuas: 0,
    puas: 0,
    kurangPuas: 0,
    tidakPuas: 0,
  });

  const getVote = async (contentType) => {
    const jumlahVote = await client
      .getSpace("he0vccpsw0h1")
      .then((space) => space.getEnvironment("master"))
      .then((environment) =>
        environment.getEntries({
          content_type: contentType,
        })
      )
      .then((response) => {
        return response.items.length;
      })
      .catch(console.error);
    return jumlahVote;
  };

  const setVote = async () => {
    const sangatPuas = await getVote("sangatPuas");
    const cukupPuas = await getVote("cukupPuas");
    const puas = await getVote("puas");
    const kurangPuas = await getVote("kurangPuas");
    const tidakPuas = await getVote("tidakPuas");
    setJumlahVote({
      sangatPuas,
      cukupPuas,
      puas,
      kurangPuas,
      tidakPuas,
    });
    setIsLoading(false);
  };

  useEffect(() => {
    console.log("get voted value");
    setVote();
  }, []);

  const [isVoted, setIsVoted] = useState(false);
  return (
    <Box
      display="grid"
      spacing={2}
      gridTemplateColumns="repeat(auto-fit, minmax(200px, 1fr))"
      gap={2}
    >
      {isLoading ? (
        <>
          <Skeleton variant="rectangular" width={210} height={118} />
          <Skeleton variant="rectangular" width={210} height={118} />
          <Skeleton variant="rectangular" width={210} height={118} />
          <Skeleton variant="rectangular" width={210} height={118} />
          <Skeleton variant="rectangular" width={210} height={118} />
        </>
      ) : (
        <VoteWrapperContext.Provider
          value={{
            jumlahVote,
            setJumlahVote,
            isVoted,
            setIsVoted,
            ipAddress,
          }}
        >
          <SangatPuas />
          <CukupPuas />
          <Puas />
          <KurangPuas />
          <TidakPuas />
        </VoteWrapperContext.Provider>
      )}
    </Box>
  );
}

export default VoteWrapper;
