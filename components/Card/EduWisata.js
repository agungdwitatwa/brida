import React from "react";
import s from "../Card/card.module.css";
import Link from "next/link";
import Image from "next/image";

function Card({ judulBerita = "judul", cover = "#", link = "#", ...props }) {
  return (
    <div className={s.container_berita}>
      <Link href={`/edukasi/edukasi-wisata/${link}`}>
        <a>
          <Image
            src={`https:${cover}`}
            {...props}
            width={"300px"}
            height={"290px"}
          />
          <div className={s.layer}>
            {judulBerita.substring(0, 55)}{" "}
            {judulBerita.length > 55 ? ". . ." : ""}
          </div>
        </a>
      </Link>
    </div>
  );
}

export default Card;
